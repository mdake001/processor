package com.futurerx;
import java.io.BufferedWriter;
import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.jdbc.StreamingJdbcTemplate;
import com.futurerx.model.dest.FuturerxProvider;
import com.futurerx.model.dest.FuturerxProviderAddress;
import com.futurerx.model.dest.FuturerxProviderAddressMedicaid;
import com.futurerx.model.dest.FuturerxProviderAddressPhone;
import com.futurerx.model.dest.FuturerxProviderAddressSPI;
import com.futurerx.model.dest.FuturerxProviderBirthDate;
import com.futurerx.model.dest.FuturerxProviderDEANumber;
import com.futurerx.model.dest.FuturerxProviderDEANumberDataWaiver;
import com.futurerx.model.dest.FuturerxProviderDeceasedData;
import com.futurerx.model.dest.FuturerxProviderDegree;
import com.futurerx.model.dest.FuturerxProviderGender;
import com.futurerx.model.dest.FuturerxProviderHCID;
import com.futurerx.model.dest.FuturerxProviderNPI;
import com.futurerx.model.dest.FuturerxProviderName;
import com.futurerx.model.dest.FuturerxProviderSpecialty;
import com.futurerx.model.dest.FuturerxProviderStateLicense;
import com.futurerx.model.dest.FuturerxProviderTaxonomy;
import com.futurerx.model.dest.FuturerxProviderUPIN;
import com.futurerx.processors.ProcessProvider;
import com.futurerx.processors.ProcessProviderAddress;
import com.futurerx.processors.ProcessProviderAddressMedicaid;
import com.futurerx.processors.ProcessProviderAddressPhone;
import com.futurerx.processors.ProcessProviderAddressSPI;
import com.futurerx.processors.ProcessProviderBirthDate;
import com.futurerx.processors.ProcessProviderDeaNumber;
import com.futurerx.processors.ProcessProviderDeaNumberDataWaiver;
import com.futurerx.processors.ProcessProviderDeceasedData;
import com.futurerx.processors.ProcessProviderDegree;
import com.futurerx.processors.ProcessProviderGender;
import com.futurerx.processors.ProcessProviderHCID;
import com.futurerx.processors.ProcessProviderNPI;
import com.futurerx.processors.ProcessProviderName;
import com.futurerx.processors.ProcessProviderSpecialty;
import com.futurerx.processors.ProcessProviderStateLicense;
import com.futurerx.processors.ProcessProviderTaxonomy;
import com.futurerx.processors.ProcessProviderUpin;
import com.futurerx.utils.FileLoadDetail;
import com.futurerx.utils.ProcessHistory;

public class FuturerxProcessor {

    private static final String PROVIDER_QUERY = "SELECT id,hcid,companyCount,deltaDate,providerId,recordType,providerType,loaderCompositeKey,dataIntakeFileId,recordNumber from futurerx_provider_stage.provider";
    
    private static final String PROVIDER_ADDRESS_QUERY = "SELECT id,address_id,hcid,address1,address2,city,company_count,county,deltaDate,dpv,geoReturn,latitude,longitude,provider_id,addrRank,recordType,state,tierCode,verificationCode,verificationDate,zip,zip4,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_address order by id asc";

    private static final String PROVIDER_GENDER_QUERY = "SELECT id,hcid,company_count,delta_date,gender,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_gender order by id asc";

    private static final String PROVIDER_DEGREE_QUERY = "SELECT id,degree,hcid,company_count,delta_date,provider_id,record_type,tier_code,verification_date,verification_code,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_degree order by id asc";
    
    private static final String PROVIDER_NAME_QUERY = "SELECT id,hcid,company_count,delta_date,first_name,last_name,middle_name,provider_id,record_type,suffix,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_name order by id asc";

    private static final String PROVIDER_NPI_QUERY = "SELECT id,hcid,company_count,deactivation_date,delta_date,npi,provider_id,npiRank,reactivation_date,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_npi order by id asc";
    
    private static final String PROVIDER_HCID_QUERY = "SELECT id,hcid,delta_date,provider_id,record_type,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_hcid order by id asc";
    
    private static final String PROVIDER_SPECIALTY_QUERY = "SELECT id,hcid,specialty_description,company_count,delta_date,provider_id,specRank,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_specialty order by id asc";
    
    private static final String PROVIDER_ADDR_MEDICAID_QUERY = "SELECT id,address_id,hcid,medicaid_id,state,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_address_medicaid order by id asc";
    
    private static final String PROVIDER_ADDR_PHONE_QUERY = "SELECT id,address_id,hcid,phone_number,phone_type,company_count,delta_date,provider_id,addrPhoneRank,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_address_phone order by id asc";
    
    private static final String PROVIDER_ADDR_SPI_QUERY = "SELECT id,address_id,hcid,active_end_date,cancel,ccr,census,spi_change,company_count,delta_date,eligibility,med_history,spi_new,provider_id,re_supp,record_type,refill,rx_fill,service_level10,service_level11,service_level12,service_level13,service_level14,service_level15,service_level_code,spi_location_code,spi_root,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber from futurerx_provider_stage.provider_address_spi order by id asc";
    
    private static final String PROVIDER_BIRTH_DATE_QUERY = "SELECT id,hcid,birth_date,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_birth_date order by id asc";
    
    private static final String PROVIDER_DEA_NUM_QUERY = "SELECT id,dea_number,hcid,company_count,dea_status_code,delta_date,drug_schedule,provider_id,record_type,renewal_date,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_dea_number order by id asc";
    
    private static final String PROVIDER_DEA_NUM_DATA_WAIVER_QUERY = "SELECT id,dea_number,hcid,activity_code,delta_date,provider_id,record_type,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_dea_number_data_waiver order by id asc";
    
    private static final String PROVIDER_DECEASED_DATA_QUERY = "SELECT id,deceased_indicator,hcid,deceased_date,delta_date,provider_id,record_type,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_deceased_data order by id asc";
    
    private static final String PROVIDER_STATE_LICENSE_QUERY = "SELECT id,hcid,license_number,license_state,company_count,delta_date,license_expiration_date,license_issue_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_state_license order by id asc";
    
    private static final String PROVIDER_TAXONOMY_QUERY = "SELECT id,hcid,taxonomy_code,company_count,delta_date,primary_taxonomy_indicator,provider_id,record_type,taxonomy_description,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_taxonomy order by id asc";
    
    private static final String PROVIDER_UPIN_QUERY = "SELECT id,hcid,upin,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey,dataIntakeFileId,recordNumber FROM futurerx_provider_stage.provider_upin order by id asc";
    
    private ProcessHistory hist = new ProcessHistory();
   
	private static String INSERT_INTAKE_ERROR_DETAIL = "INSERT INTO data_loading_audit.file_load_detail (file_load_summary_id, record_number, status, remark) VALUES (?, ?, ?, ?)";
	
	private static String UPDATE_INTAKE_PROCESS_HISTORY_QUERY = "UPDATE data_loading_audit.file_load_summary SET count_received = ?, count_updated_medhok = ?, count_failed_medhok = ? WHERE id = ?";                                                                   

	private ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final Logger log = LoggerFactory.getLogger(FuturerxProcessor.class);
	private ProcessingCounts counts = new ProcessingCounts();

	private List<Future<ProcessingCounts>> threadResults = new ArrayList<Future<ProcessingCounts>>();
	
	private Integer batchSize = 200;
	private Integer threadPoolSize = 30;
	private Boolean trackData = false;

	private ExecutorService pool = null;

	private List<FuturerxProvider> providersBatch = new ArrayList<FuturerxProvider>();
	private List<FuturerxProviderAddress> providerAddressBatch = new ArrayList<FuturerxProviderAddress>();
	private List<FuturerxProviderGender> providerGenderBatch = new ArrayList<FuturerxProviderGender>();
	private List<FuturerxProviderName> providerNameBatch = new ArrayList<FuturerxProviderName>();
	private List<FuturerxProviderAddressMedicaid> providerAddrMedicaidBatch = new ArrayList<FuturerxProviderAddressMedicaid>();
	private List<FuturerxProviderAddressPhone> providerAddrPhoneBatch = new ArrayList<FuturerxProviderAddressPhone>();
	private List<FuturerxProviderAddressSPI> providerAddrSPIBatch = new ArrayList<FuturerxProviderAddressSPI>();
	private List<FuturerxProviderBirthDate> providerBirthDateBatch = new ArrayList<FuturerxProviderBirthDate>();
	private List<FuturerxProviderDEANumber> providerDEANumberBatch = new ArrayList<FuturerxProviderDEANumber>();
	private List<FuturerxProviderDEANumberDataWaiver> providerDEANumDataWaiverBatch = new ArrayList<FuturerxProviderDEANumberDataWaiver>();
	private List<FuturerxProviderDeceasedData> providerDeceasedDataBatch = new ArrayList<FuturerxProviderDeceasedData>();
	private List<FuturerxProviderHCID> providerHCIDBatch = new ArrayList<FuturerxProviderHCID>();
	private List<FuturerxProviderNPI> providerNPIBatch = new ArrayList<FuturerxProviderNPI>();
	private List<FuturerxProviderSpecialty> providerSpecialtyBatch = new ArrayList<FuturerxProviderSpecialty>();
	private List<FuturerxProviderStateLicense> providerStateLicenseBatch = new ArrayList<FuturerxProviderStateLicense>();
	private List<FuturerxProviderTaxonomy> providerTaxonomyBatch = new ArrayList<FuturerxProviderTaxonomy>();
	private List<FuturerxProviderUPIN> providerUPINBatch = new ArrayList<FuturerxProviderUPIN>();
	private List<FuturerxProviderDegree> providerDegreeBatch = new ArrayList<FuturerxProviderDegree>();
	
	
	private File errorFilesDirectory = null;
	private Map<String, BufferedWriter> errorFileWriters = new HashMap<String, BufferedWriter>();
	
	private Date processingDateTime = new Date();
	private long size = 0;
	private String limit = "";
	public static Integer companyId = -1;
				
	private void processProvider() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Loader", null);
		String sub[] = PROVIDER_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 2] + " " + sub[sub.length - 1];
	    size = jdbcTemplate.queryForLong(query);

		jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProvider p = new FuturerxProvider();
					p.setHcid(rs.getString("hcid"));
					p.setCompanyCount(rs.getString("companyCount"));
					p.setDeltaDate(rs.getDate("deltaDate"));
					p.setProviderId(rs.getLong("providerId"));
					p.setRecordType(rs.getString("recordType"));
					p.setProviderType(rs.getString("providerType"));
					p.setLoaderCompositeKey(rs.getString("hcid"));
					p.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					p.setRecordNumber(rs.getLong("recordNumber"));
					
					providersBatch.add(p);
					if (providersBatch.size()==batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providersBatch, processingDateTime, ProcessProvider.class);
						threadResults.add(pool.submit(c));						
						providersBatch = new ArrayList<FuturerxProvider>();
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						processResults();
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providersBatch.size()>0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providersBatch, processingDateTime, ProcessProvider.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderAddress() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Address Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_ADDRESS_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_ADDRESS_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderAddress pa = new FuturerxProviderAddress();
					pa.setHcid(rs.getString("hcid"));
					pa.setAddressId(rs.getString("address_id"));
					pa.setAddress1(rs.getString("address1"));
					pa.setAddress2(rs.getString("address2"));
					pa.setCity(rs.getString("city"));
					pa.setCompanyCount(rs.getString("company_count"));
					pa.setCounty(rs.getString("county"));
					pa.setDeltaDate(rs.getDate("deltaDate"));
					pa.setDpv(rs.getString("dpv"));
					pa.setGeoReturn(rs.getString("geoReturn"));
					pa.setLatitude(rs.getString("latitude"));
					pa.setLongitude(rs.getString("longitude"));
					pa.setProviderId(rs.getString("provider_id"));
					pa.setAddrRank(rs.getString("addrRank"));
					pa.setRecordType(rs.getString("recordType"));
					pa.setState(rs.getString("state"));
					pa.setTierCode(rs.getString("tierCode"));
					pa.setVerificationCode(rs.getString("verificationCode"));
					pa.setVerificationDate(rs.getDate("verificationDate"));
					pa.setZip(rs.getString("zip"));
					pa.setZip4(rs.getString("zip4"));
					pa.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pa.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pa.setRecordNumber(rs.getLong("recordNumber"));
					
					providerAddressBatch.add(pa);
					if (providerAddressBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerAddressBatch, processingDateTime, ProcessProviderAddress.class);
						threadResults.add(pool.submit(c));
						providerAddressBatch = new ArrayList<FuturerxProviderAddress>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerAddressBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerAddressBatch, processingDateTime, ProcessProviderAddress.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderAddressMedicaid() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Address Medicaid Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_ADDR_MEDICAID_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_ADDR_MEDICAID_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderAddressMedicaid pa = new FuturerxProviderAddressMedicaid();
					pa.setHcid(rs.getString("hcid"));
					pa.setAddressId(rs.getString("address_id"));
					pa.setMedicaidId(rs.getString("medicaid_id"));
					pa.setCompanyCount(rs.getString("company_count"));
					pa.setState(rs.getString("state"));
					pa.setDeltaDate(rs.getDate("delta_date"));
					pa.setProviderId(rs.getString("provider_id"));
					pa.setRecordType(rs.getString("record_Type"));
					pa.setTierCode(rs.getString("tier_Code"));
					pa.setVerificationCode(rs.getString("verification_Code"));
					pa.setVerificationDate(rs.getDate("verification_Date"));
					pa.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pa.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pa.setRecordNumber(rs.getLong("recordNumber"));
					
					providerAddrMedicaidBatch.add(pa);
					if (providerAddrMedicaidBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerAddrMedicaidBatch, processingDateTime, ProcessProviderAddressMedicaid.class);
						threadResults.add(pool.submit(c));
						providerAddrMedicaidBatch = new ArrayList<FuturerxProviderAddressMedicaid>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerAddrMedicaidBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerAddrMedicaidBatch, processingDateTime, ProcessProviderAddressMedicaid.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderAddressPhone() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Address Phone Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_ADDR_PHONE_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_ADDR_PHONE_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderAddressPhone pa = new FuturerxProviderAddressPhone();
					pa.setHcid(rs.getString("hcid"));
					pa.setAddressId(rs.getString("address_id"));
					pa.setPhoneNumber(rs.getString("phone_number"));
					pa.setPhoneType(rs.getString("phone_type"));
					pa.setAddrPhoneRank(rs.getString("addrPhoneRank"));
					pa.setCompanyCount(rs.getString("company_count"));
					pa.setDeltaDate(rs.getDate("delta_date"));
					pa.setProviderId(rs.getString("provider_id"));
					pa.setRecordType(rs.getString("record_Type"));
					pa.setTierCode(rs.getString("tier_Code"));
					pa.setVerificationCode(rs.getString("verification_Code"));
					pa.setVerificationDate(rs.getDate("verification_Date"));
					pa.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pa.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pa.setRecordNumber(rs.getLong("recordNumber"));
					
					providerAddrPhoneBatch.add(pa);
					if (providerAddrPhoneBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerAddrPhoneBatch, processingDateTime, ProcessProviderAddressPhone.class);
						threadResults.add(pool.submit(c));
						providerAddrPhoneBatch = new ArrayList<FuturerxProviderAddressPhone>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerAddrPhoneBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerAddrPhoneBatch, processingDateTime, ProcessProviderAddressPhone.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderAddressSPI() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Address SPI Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_ADDR_SPI_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_ADDR_SPI_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderAddressSPI pa = new FuturerxProviderAddressSPI();
					pa.setHcid(rs.getString("hcid"));
					pa.setAddressId(rs.getString("address_id"));
					pa.setActiveEndDate(rs.getDate("active_end_date"));
					pa.setCancel(rs.getString("cancel"));
					pa.setCcr(rs.getString("ccr"));
					pa.setCensus(rs.getDate("census"));
					pa.setChange(rs.getString("spi_change"));
					pa.setEligibility(rs.getString("eligibility")); 
					pa.setMedHistory(rs.getString("med_history"));
					pa.setNpiNew(rs.getString("spi_new")); 
					pa.setReSupp(rs.getString("re_supp"));
					pa.setRefill(rs.getString("refill"));
					pa.setRxFill(rs.getString("rx_fill"));
					pa.setServiceLevel10(rs.getString("service_level10"));
					pa.setServiceLevel11(rs.getString("service_level11"));
					pa.setServiceLevel12(rs.getString("service_level12"));
					pa.setServiceLevel13(rs.getString("service_level13"));
					pa.setServiceLevel14(rs.getString("service_level14"));
					pa.setServiceLevel15(rs.getString("service_level15"));
					pa.setServiceLevelCode(rs.getString("service_level_code"));
					pa.setSpiLocationCode(rs.getString("spi_location_code"));
					pa.setSpiRoot(rs.getString("spi_root"));
					pa.setCompanyCount(rs.getString("company_count"));
					pa.setDeltaDate(rs.getDate("delta_date"));
					pa.setProviderId(rs.getString("provider_id"));
					pa.setRecordType(rs.getString("record_Type"));
					pa.setTierCode(rs.getString("tier_Code"));
					pa.setVerificationCode(rs.getString("verification_Code"));
					pa.setVerificationDate(rs.getDate("verification_Date"));
					pa.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pa.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pa.setRecordNumber(rs.getLong("recordNumber"));
					
					providerAddrSPIBatch.add(pa);
					if (providerAddrSPIBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerAddrSPIBatch, processingDateTime, ProcessProviderAddressSPI.class);
						threadResults.add(pool.submit(c));
						providerAddrSPIBatch = new ArrayList<FuturerxProviderAddressSPI>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerAddrSPIBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerAddrSPIBatch, processingDateTime, ProcessProviderAddressSPI.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderBirthDate() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider BirthDate Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_BIRTH_DATE_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_BIRTH_DATE_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderBirthDate pb = new FuturerxProviderBirthDate();
					pb.setHcid(rs.getString("hcid"));
					pb.setCompanyCount(rs.getString("company_count"));
					pb.setDeltaDate(rs.getDate("delta_date"));
					pb.setBirthDate(rs.getDate("birth_date"));
					pb.setProviderId(rs.getString("provider_id"));
					pb.setRecordType(rs.getString("record_type"));
					pb.setTierCode(rs.getString("tier_code"));
					pb.setVerificationCode(rs.getString("verification_code"));
					pb.setVerificationDate(rs.getDate("verification_date"));
					pb.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pb.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pb.setRecordNumber(rs.getLong("recordNumber"));
					
					providerBirthDateBatch.add(pb);
					if (providerBirthDateBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerBirthDateBatch, processingDateTime, ProcessProviderBirthDate.class);
						threadResults.add(pool.submit(c));
						providerBirthDateBatch = new ArrayList<FuturerxProviderBirthDate>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerBirthDateBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerBirthDateBatch, processingDateTime, ProcessProviderBirthDate.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	

	private void processProviderDEANumber() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider DEANumber Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_DEA_NUM_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_DEA_NUM_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderDEANumber pt = new FuturerxProviderDEANumber();
					pt.setHcid(rs.getString("hcid"));
					pt.setCompanyCount(rs.getString("company_count"));
					pt.setDeaNumber(rs.getString("dea_number"));
					pt.setDeaStatusCode(rs.getString("dea_status_code"));
					pt.setRenewalDate(rs.getDate("renewal_date"));
					pt.setDrugSchedule(rs.getString("drug_schedule"));
					pt.setDeltaDate(rs.getDate("delta_date"));
					pt.setProviderId(rs.getString("provider_id"));
					pt.setRecordType(rs.getString("record_type"));
					pt.setTierCode(rs.getString("tier_code"));
					pt.setVerificationCode(rs.getString("verification_code"));
					pt.setVerificationDate(rs.getDate("verification_date"));
					pt.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pt.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pt.setRecordNumber(rs.getLong("recordNumber"));
					
					providerDEANumberBatch.add(pt);
					if (providerDEANumberBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerDEANumberBatch, processingDateTime, ProcessProviderDeaNumber.class);
						threadResults.add(pool.submit(c));
						providerDEANumberBatch = new ArrayList<FuturerxProviderDEANumber>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerDEANumberBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerDEANumberBatch, processingDateTime, ProcessProviderDeaNumber.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

	

	private void processProviderDEANumberDataWaiver() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider DEANumberDW Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_DEA_NUM_DATA_WAIVER_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_DEA_NUM_DATA_WAIVER_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderDEANumberDataWaiver pw = new FuturerxProviderDEANumberDataWaiver();
					pw.setHcid(rs.getString("hcid"));
					pw.setActivityCode(rs.getString("activity_code"));
					pw.setDeaNumber(rs.getString("dea_number"));
					pw.setDeltaDate(rs.getDate("delta_date"));
					pw.setProviderId(rs.getString("provider_id"));
					pw.setRecordType(rs.getString("record_type"));
					pw.setVerificationDate(rs.getDate("verification_date"));
					pw.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pw.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pw.setRecordNumber(rs.getLong("recordNumber"));
					
					
					providerDEANumDataWaiverBatch.add(pw);
					if (providerDEANumDataWaiverBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerDEANumDataWaiverBatch, processingDateTime, ProcessProviderDeaNumberDataWaiver.class);
						threadResults.add(pool.submit(c));
						providerDEANumDataWaiverBatch = new ArrayList<FuturerxProviderDEANumberDataWaiver>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
//						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerDEANumDataWaiverBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerDEANumDataWaiverBatch, processingDateTime, ProcessProviderDeaNumberDataWaiver.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

	

	private void processProviderDeceasedData() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider DeceasedData Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_DECEASED_DATA_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_DECEASED_DATA_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderDeceasedData pt = new FuturerxProviderDeceasedData();
					pt.setHcid(rs.getString("hcid"));
					pt.setDeceasedIndicator(rs.getString("deceased_indicator"));
					pt.setDeltaDate(rs.getDate("delta_date"));
					pt.setDeceasedDate(rs.getDate("deceased_date"));
					pt.setProviderId(rs.getString("provider_id"));
					pt.setRecordType(rs.getString("record_type"));
					pt.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pt.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pt.setRecordNumber(rs.getLong("recordNumber"));
					
					providerDeceasedDataBatch.add(pt);
					if (providerDeceasedDataBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerDeceasedDataBatch, processingDateTime, ProcessProviderDeceasedData.class);
						threadResults.add(pool.submit(c));
						providerDeceasedDataBatch = new ArrayList<FuturerxProviderDeceasedData>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerDeceasedDataBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerDeceasedDataBatch, processingDateTime, ProcessProviderDeceasedData.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

	
	
	private void processProviderGender() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Gender Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_GENDER_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_GENDER_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderGender pg = new FuturerxProviderGender();
					pg.setHcid(rs.getString("hcid"));
					pg.setCompanyCount(rs.getString("company_count"));
					pg.setDeltaDate(rs.getDate("delta_date"));
					pg.setGender(rs.getString("gender"));
					pg.setProviderId(rs.getString("provider_id"));
					pg.setRecordType(rs.getString("record_type"));
					pg.setTierCode(rs.getString("tier_code"));
					pg.setVerificationCode(rs.getString("verification_code"));
					pg.setVerificationDate(rs.getDate("verification_date"));
					pg.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pg.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pg.setRecordNumber(rs.getLong("recordNumber"));
					
					providerGenderBatch.add(pg);
					if (providerGenderBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerGenderBatch, processingDateTime, ProcessProviderGender.class);
						threadResults.add(pool.submit(c));
						providerGenderBatch = new ArrayList<FuturerxProviderGender>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerGenderBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerGenderBatch, processingDateTime, ProcessProviderGender.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderDegree() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Degree Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_DEGREE_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_DEGREE_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderDegree pd = new FuturerxProviderDegree();
					pd.setDegree(rs.getString("degree"));
					pd.setHcid(rs.getString("hcid"));
					pd.setCompanyCount(rs.getString("company_count"));
					pd.setDeltaDate(rs.getDate("delta_date"));
					pd.setProviderId(rs.getString("provider_id"));
					pd.setRecordType(rs.getString("record_type"));
					pd.setTierCode(rs.getString("tier_code"));
					pd.setVerificationDate(rs.getDate("verification_date"));
					pd.setVerificationCode(rs.getString("verification_code"));
					pd.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pd.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pd.setRecordNumber(rs.getLong("recordNumber"));
					
					providerDegreeBatch.add(pd);
					if (providerDegreeBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerDegreeBatch, processingDateTime, ProcessProviderDegree.class);
						threadResults.add(pool.submit(c));
						providerDegreeBatch = new ArrayList<FuturerxProviderDegree>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerDegreeBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerDegreeBatch, processingDateTime, ProcessProviderDegree.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderName() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Name Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_NAME_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_NAME_QUERY, new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderName pn = new FuturerxProviderName();
					pn.setHcid(rs.getString("hcid"));
					pn.setCompanyCount(rs.getString("company_count"));
					pn.setDeltaDate(rs.getDate("delta_date"));
					pn.setFirstName(rs.getString("first_name"));
					pn.setMiddleName(rs.getString("middle_name"));
					pn.setLastName(rs.getString("last_name"));
					pn.setProviderId(rs.getString("provider_id"));
					pn.setRecordType(rs.getString("record_type"));
					pn.setSuffix(rs.getString("suffix"));
					pn.setTierCode(rs.getString("tier_code"));
					pn.setVerificationCode(rs.getString("verification_code"));
					pn.setVerificationDate(rs.getDate("verification_date"));
					pn.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pn.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pn.setRecordNumber(rs.getLong("recordNumber"));
					
					providerNameBatch.add(pn);
					if (providerNameBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerNameBatch, processingDateTime, ProcessProviderName.class);
						threadResults.add(pool.submit(c));
						providerNameBatch = new ArrayList<FuturerxProviderName>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerNameBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerNameBatch, processingDateTime, ProcessProviderName.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderNPI() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider NPI Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_NPI_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_NPI_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderNPI pn = new FuturerxProviderNPI();
					pn.setHcid(rs.getString("hcid"));
					pn.setCompanyCount(rs.getString("company_count"));
					pn.setDeactivationDate(rs.getDate("deactivation_date"));
					pn.setDeltaDate(rs.getDate("delta_date"));
					pn.setNpi(rs.getString("npi"));
					pn.setNpiRank(rs.getString("npiRank"));
					pn.setReactivationDate(rs.getDate("reactivation_date"));
					pn.setProviderId(rs.getString("provider_id"));
					pn.setRecordType(rs.getString("record_type"));
					pn.setTierCode(rs.getString("tier_code"));
					pn.setVerificationCode(rs.getString("verification_code"));
					pn.setVerificationDate(rs.getDate("verification_date"));
					pn.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pn.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pn.setRecordNumber(rs.getLong("recordNumber"));
					
					providerNPIBatch.add(pn);
					if (providerNPIBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerNPIBatch, processingDateTime, ProcessProviderNPI.class);
						threadResults.add(pool.submit(c));
						providerNPIBatch = new ArrayList<FuturerxProviderNPI>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerNPIBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerNPIBatch, processingDateTime, ProcessProviderNPI.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderSpecialty() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Specialty Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_SPECIALTY_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_SPECIALTY_QUERY, new RowCallbackHandler() {
			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderSpecialty ps = new FuturerxProviderSpecialty();
					ps.setHcid(rs.getString("hcid"));
					ps.setSpecialtyDescription(rs.getString("specialty_description"));
					ps.setCompanyCount(rs.getString("company_count"));
					ps.setDeltaDate(rs.getDate("delta_date"));
					ps.setProviderId(rs.getString("provider_id"));
					ps.setSpecialtyRank(rs.getString("specRank"));
					ps.setRecordType(rs.getString("record_type"));
					ps.setTierCode(rs.getString("tier_code"));
					ps.setVerificationCode(rs.getString("verification_code"));
					ps.setVerificationDate(rs.getDate("verification_date"));
					ps.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					ps.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					ps.setRecordNumber(rs.getLong("recordNumber"));
					
					providerSpecialtyBatch.add(ps);
					if (providerSpecialtyBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerSpecialtyBatch, processingDateTime, ProcessProviderSpecialty.class);
						threadResults.add(pool.submit(c));
						providerSpecialtyBatch = new ArrayList<FuturerxProviderSpecialty>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerSpecialtyBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerSpecialtyBatch, processingDateTime, ProcessProviderSpecialty.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

	private void processProviderStateLicense() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider StateLicense Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_STATE_LICENSE_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_STATE_LICENSE_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderStateLicense ps = new FuturerxProviderStateLicense();
					ps.setHcid(rs.getString("hcid"));
					ps.setCompanyCount(rs.getString("company_count"));
					ps.setLicenseExpirationDate(rs.getDate("license_expiration_date"));
					ps.setDeltaDate(rs.getDate("delta_date"));
					ps.setLicenseNumber(rs.getString("license_number"));
					ps.setLicenseState(rs.getString("license_state"));
					ps.setLicenseIssueDate(rs.getDate("license_issue_date"));
					ps.setProviderId(rs.getString("provider_id"));
					ps.setRecordType(rs.getString("record_type"));
					ps.setTierCode(rs.getString("tier_code"));
					ps.setVerificationCode(rs.getString("verification_code"));
					ps.setVerificationDate(rs.getDate("verification_date"));
					ps.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					ps.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					ps.setRecordNumber(rs.getLong("recordNumber"));
					
					providerStateLicenseBatch.add(ps);
					if (providerStateLicenseBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerStateLicenseBatch, processingDateTime, ProcessProviderStateLicense.class);
						threadResults.add(pool.submit(c));
						providerStateLicenseBatch = new ArrayList<FuturerxProviderStateLicense>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerStateLicenseBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerStateLicenseBatch, processingDateTime, ProcessProviderStateLicense.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	
	private void processProviderTaxonomy() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider Taxonomy Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_TAXONOMY_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_TAXONOMY_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderTaxonomy pt = new FuturerxProviderTaxonomy();
					pt.setHcid(rs.getString("hcid"));
					pt.setCompanyCount(rs.getString("company_count"));
					pt.setTaxonomyCode(rs.getString("taxonomy_code"));
					pt.setDeltaDate(rs.getDate("delta_date"));
					pt.setPrimaryTaxonomyIndicator(rs.getString("primary_taxonomy_indicator"));
					pt.setTaxonomyDescription(rs.getString("taxonomy_description"));
					pt.setProviderId(rs.getString("provider_id"));
					pt.setRecordType(rs.getString("record_type"));
					pt.setTierCode(rs.getString("tier_code"));
					pt.setVerificationCode(rs.getString("verification_code"));
					pt.setVerificationDate(rs.getDate("verification_date"));
					pt.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pt.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pt.setRecordNumber(rs.getLong("recordNumber"));
					
					providerTaxonomyBatch.add(pt);
					if (providerTaxonomyBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerTaxonomyBatch, processingDateTime, ProcessProviderTaxonomy.class);
						threadResults.add(pool.submit(c));
						providerTaxonomyBatch = new ArrayList<FuturerxProviderTaxonomy>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerTaxonomyBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerTaxonomyBatch, processingDateTime, ProcessProviderTaxonomy.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

	private void processProviderHCID() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider HCID Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_HCID_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_HCID_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderHCID ph = new FuturerxProviderHCID();
					ph.setHcid(rs.getString("hcid"));
					ph.setDeltaDate(rs.getDate("delta_date"));
					ph.setProviderId(rs.getString("provider_id"));
					ph.setRecordType(rs.getString("record_type"));
					ph.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					ph.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					ph.setRecordNumber(rs.getLong("recordNumber"));
					
					providerHCIDBatch.add(ph);
					if (providerHCIDBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerHCIDBatch, processingDateTime, ProcessProviderHCID.class);
						threadResults.add(pool.submit(c));
						providerHCIDBatch = new ArrayList<FuturerxProviderHCID>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerHCIDBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerHCIDBatch, processingDateTime, ProcessProviderHCID.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}
	

	private void processProviderUPIN() throws Exception {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		hist.setJdbcTemplate(jdbcTemplate);
		hist.startProcessing("Futurerx Provider UPIN Loader", null);

		counts = new ProcessingCounts();
        final Set<Long> stDataIntakeIDs = new HashSet<Long>();
		String sub[] = PROVIDER_UPIN_QUERY.split(" ");
		String query = sub[0] + " COUNT(*) " + sub[sub.length - 6] + " " + sub[sub.length - 5];
	    size = jdbcTemplate.queryForLong(query);
		
	    jdbcTemplate.setFetchSize(Integer.MIN_VALUE);
		jdbcTemplate.query(PROVIDER_UPIN_QUERY, new RowCallbackHandler() {

			public void processRow(ResultSet rs) throws SQLException {
				try {
					counts.incrementTotal();
					FuturerxProviderUPIN pt = new FuturerxProviderUPIN();
					pt.setHcid(rs.getString("hcid"));
					pt.setCompanyCount(rs.getString("company_count"));
					pt.setUpin(rs.getString("upin"));
					pt.setDeltaDate(rs.getDate("delta_date"));
					pt.setProviderId(rs.getString("provider_id"));
					pt.setRecordType(rs.getString("record_type"));
					pt.setTierCode(rs.getString("tier_code"));
					pt.setVerificationCode(rs.getString("verification_code"));
					pt.setVerificationDate(rs.getDate("verification_date"));
					pt.setLoaderCompositeKey(rs.getString("loaderCompositeKey"));
					pt.setDataIntakeFileId(rs.getInt("dataIntakeFileId"));
					pt.setRecordNumber(rs.getLong("recordNumber"));
					
					providerUPINBatch.add(pt);
					if (providerUPINBatch.size() >= batchSize ) {
						Callable<ProcessingCounts> c = new ProcessingThread(providerUPINBatch, processingDateTime, ProcessProviderUpin.class);
						threadResults.add(pool.submit(c));
						providerUPINBatch = new ArrayList<FuturerxProviderUPIN>();						
						System.out.println("About " + ((float)Math.round((float)(counts.getTotalRecords())/(float)size*10000))/100 + "% done. ");
					}
					while (threadResults.size() > threadPoolSize) {
						System.out.println("Thread Results Size: " + threadResults.size());
						processResults();							
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					counts.incrementFailed();
					counts.setException(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}});
		if (providerUPINBatch.size() > 0) {
			Callable<ProcessingCounts> c = new ProcessingThread(providerUPINBatch, processingDateTime, ProcessProviderUpin.class);
			threadResults.add(pool.submit(c));						
		}
		while (threadResults.size() > 0) {
			processResults();							
			Thread.sleep(1000);
		}

		hist.endProcessing(counts);
		updateDataIntakeSummary(stDataIntakeIDs);
	}

//		public void FileLoadHistory(Date processingTime, Long fileLoadSummaryId, String loadType){
//		
//		JdbcTemplate jdbcTemplate =  (JdbcTemplate) aContext.getBean("jdbcTemplate");
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		List<Long> idList=new ArrayList<Long>();
//		ResultSet medhokId=null;
//		Integer trackRecordCount=0;
//		Integer i=null;
//		try{
//			
//		i=jdbcTemplate.queryForInt("select count(*) from Futurerx.file_load_history where file_load_summary_id = ?", fileLoadSummaryId);
//		if (i != 0){
//			jdbcTemplate.update("delete from Futurerx.file_load_history where file_load_summary_id = ?" , fileLoadSummaryId);
//		}
//		String tableName = null;
//		if(loadType.equalsIgnoreCase("rx_claim")){
//			tableName ="Futurerx.pharmacy_claims";
//		}else if (loadType.equalsIgnoreCase("claim_header")){
//			tableName = "Futurerx.member_claims";
//		}else if (loadType.equalsIgnoreCase("member_lab_service")){
//			tableName = "Futurerx.member_labdata";
//		}
//		
//		final String FIND_FUTURERX_ID_QUERY = "Select id from "+ tableName +" where update_datetime =";
//		
//		final StringBuilder queryBuilder = new StringBuilder(FIND_MEDHOK_ID_QUERY);
//		queryBuilder.append("'" + (sdf.format(processingTime)) + "'");
//		String sql = (queryBuilder).toString();
//		
//		
//		Connection conn = jdbcTemplate.getDataSource().getConnection();
//		PreparedStatement ps = conn.prepareStatement(sql);
//		
//		String INSERT_FILE_LOAD_HISTORY="Insert into Futurerx.file_load_history set file_load_summary_id ='" + fileLoadSummaryId + "', processing_time ='"+ (sdf.format(processingTime)) + "', load_type ='"+ tableName +"', medhok_id = ?";
//		PreparedStatement insert=conn.prepareStatement(INSERT_FILE_LOAD_HISTORY);
//		
//		medhokId = ps.executeQuery(); 
//		while(medhokId.next()) {
//			int rows=0;
//			long id = medhokId.getLong(1);
//			if (!idList.contains(id)){
//				idList.add(id);
//			}
//			if (idList.size() == batchSize){
//				int j =0;
//				while (j <idList.size()){
//					insert.setLong(1,idList.get(j));
//					insert.executeUpdate();
//					trackRecordCount++;
//					j++;
//				}
//				
//				//conn.commit();
//				idList.clear();
//			}
//		}
//		
//		int j =0;
//		while (j < idList.size()){
//			insert.setLong(1,idList.get(j));
//			insert.executeUpdate();
//			trackRecordCount++;
//			j++;
//		}
//		
//		//conn.commit();
//		idList.clear();
//		System.out.println("Data Tracking Ends. Total number of records backed up = " + trackRecordCount);
//		}
//		catch (Exception e){
//			e.printStackTrace();
//		}
//	}

	private void updateDataIntakeSummary(Set<Long> stDataIntakeIDs) {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");
		for (Long id : stDataIntakeIDs) {
			jdbcTemplate.update(UPDATE_INTAKE_PROCESS_HISTORY_QUERY, counts.getInsertedRecords(), counts.getUpdatedRecords(), counts.getFailedRecords(), id);
		}
		counts = new ProcessingCounts();
	}//count_inserted_medhok = ?, count_updated_medhok = ?, count_failed_medhok = ? WHERE id = ?


	public void finishProcessing() {
		pool.shutdown();
		
		for (String filename: errorFileWriters.keySet()) {
			try {
				log.info("Closing Error File: " + filename);
				BufferedWriter writer = errorFileWriters.get(filename);
				writer.close();
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}
	
	public void initialize() {
		this.pool = Executors.newFixedThreadPool(threadPoolSize);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
    	long startTime = System.currentTimeMillis();
		Options opts = new Options();
		opts.addOption("help", false, "Print this message.");
		
		opts.addOption(OptionBuilder.withLongOpt("errordir").withDescription("Error Files Directory").hasArg().withArgName("DIR").create());
		opts.addOption(OptionBuilder.withLongOpt("batch-size").hasArg().withArgName("BATCH_SIZE").withDescription("Batch Size").create());
		opts.addOption(OptionBuilder.withLongOpt("thread-pool-size").hasArg().withArgName("POOL_SIZE").withDescription("Thread Pool Size").create());
		
		opts.addOption("sql","sql", false,"runSql");
		opts.addOption("limit", "start-end", true, "Continue from and to separated by ','");
		opts.addOption("track", "track-data",false, "fileLoadHistory");
		opts.addOption("a", "all", false, "Process All");
		
		opts.addOption("p", "provider", false, "Process Providers");
		opts.addOption("pa", "provider-address", false, "Process Provider Addresses");
		opts.addOption("pam", "provider-address-medicaid", false, "Process Providers Address Medicaid");
		opts.addOption("pap", "provider-address-phone", false, "Process Provider Address Phone");
		opts.addOption("pas", "provider-address-spi", false, "Process Provider Address SPI");
		opts.addOption("pbd", "provider-birth-date", false, "Process Provider Birth Date");
		opts.addOption("pdn", "provider-dea-number", false, "Process Provider DEA Number");
		opts.addOption("pdw", "provider-dea-number-data-waiver", false, "Process Provider DEA Number Data Waiver");
		opts.addOption("pdd", "provider-deceased-data", false, "Process Provider Deceased Data");
		opts.addOption("pd", "provider-degree", false, "Process Provider Degree");
		opts.addOption("pg", "provider-gender", false, "Process Provider Gender");
		opts.addOption("ph", "provider-hcid", false, "Process Provider HCID");
		opts.addOption("pn", "provider-name", false, "Process Provider Name");
		opts.addOption("pnp", "provider-npi", false, "Process Provider NPI");
		opts.addOption("ps", "provider-specialty", false, "Process Provider Specialty");
		opts.addOption("psl", "provider-state-license", false, "Process Provider State License");
		opts.addOption("pt", "provider-taxonomy", false, "Process Provider Taxonomy");
		opts.addOption("pu", "provider-upin", false, "Process Provider UPIN");
		
		CommandLineParser parser = new GnuParser();
		
		FuturerxProcessor m = null;
		try {
	        // parse the command line arguments
	        CommandLine line = parser.parse( opts, args );

	        if (line.hasOption("help")) {
	    		// automatically generate the help statement
	    		HelpFormatter formatter = new HelpFormatter();
	    		formatter.printHelp( "java -jar Futurerx-processor.jar", opts );
	    		return;
	        }

	        m = new FuturerxProcessor();
	        if (line.hasOption("limit")) {
	        	m.limit = line.getOptionValue("limit");
	        }
	        if (line.hasOption("cid")) {
	        	m.companyId = Integer.parseInt(line.getOptionValue("cid"));
	        }
	        if (line.hasOption("errordir")) {
        		log.debug("errordir: {}", line.getOptionValue("errordir"));
	        	String errorDir = line.getOptionValue("errordir");
	        	m.setErrorFilesDirectory(new File(errorDir));
	        }
	        if(line.hasOption("batch-size")) {
	        	try {
	        		log.debug("batchSize: {}", line.getOptionValue("batch-size"));
	        		m.setBatchSize(Integer.parseInt(line.getOptionValue("batch-size")));
	        	} catch (Exception e) {
	        		throw new Exception("Invalid Batch Size option value.", e);
	        	}
	        }
	        if(line.hasOption("thread-pool-size")) {
	        	try {
	        		log.debug("threadPoolSize: {}", line.getOptionValue("thread-pool-size"));
	        		m.setThreadPoolSize(Integer.parseInt(line.getOptionValue("thread-pool-size")));
	        	} catch (Exception e) {
	        		throw new Exception("Invalid Thread Pool Size option value", e);
	        	}
	        }
	        if (line.hasOption("track")){
	        		log.info("Tracking Data");
	        		m.setTrackData(true);
	        }
	        m.initialize();
	        //TODO: Processors starts here. 
	        Profiler profiler = new Profiler("Futurerx Processor");
	        if (line.hasOption("all") || line.hasOption("p") || line.hasOption("provider")) {
		        profiler.start("provider");
	        	m.processProvider();		
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pa") || line.hasOption("provider-address")) {
		        profiler.start("providerAddress");
	        	m.processProviderAddress();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pam") || line.hasOption("provider-address-medicaid")) {
		        profiler.start("providerAddressMedicaid");
	        	m.processProviderAddressMedicaid();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pap") || line.hasOption("provider-address-phone")) {
		        profiler.start("providerAddressPhone");
	        	m.processProviderAddressPhone();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pas") || line.hasOption("provider-address-spi")) {
		        profiler.start("providerAddressSPI");
	        	m.processProviderAddressSPI();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pbd") || line.hasOption("provider-birth-date")) {
		        profiler.start("providerBirthDate");
	        	m.processProviderBirthDate();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pdn") || line.hasOption("provider-dea-number")) {
		        profiler.start("providerDEANumber");
	        	m.processProviderDEANumber();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pdw") || line.hasOption("provider-dea-number-data-waiver")) {
		        profiler.start("providerDEANumberDataWaiver");
	        	m.processProviderDEANumberDataWaiver();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pdd") || line.hasOption("provider-deceased-data")) {
		        profiler.start("providerDeceasedData");
	        	m.processProviderDeceasedData();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pd") || line.hasOption("provider-degree")) {
		        profiler.start("providerDegree");
	        	m.processProviderDegree();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pg") || line.hasOption("provider-gender")) {
		        profiler.start("providerGender");
	        	m.processProviderGender();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("ph") || line.hasOption("provider-hcid")) {
		        profiler.start("providerHCID");
	        	m.processProviderHCID();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pn") || line.hasOption("provider-name")) {
		        profiler.start("providerName");
	        	m.processProviderName();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pnp") || line.hasOption("provider-npi")) {
		        profiler.start("providerNPI");
	        	m.processProviderNPI();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("ps") || line.hasOption("provider-specialty")) {
		        profiler.start("providerSpecialty");
	        	m.processProviderSpecialty();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("psl") || line.hasOption("provider-state-license")) {
		        profiler.start("providerStateLicense");
	        	m.processProviderStateLicense();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pt") || line.hasOption("provider-taxonomy")) {
		        profiler.start("providerTaxonomy");
	        	m.processProviderTaxonomy();
		        profiler.stop().print();
	        }
	        if (line.hasOption("all") || line.hasOption("pu") || line.hasOption("provider-upin")) {
		        profiler.start("providerUpin");
	        	m.processProviderUPIN();
		        profiler.stop().print();
	        }
	    } catch (ParseException exp ) {
	        log.error( "Parsing failed. Reason: " + exp.getMessage() );
	        log.error(ExceptionUtils.getFullStackTrace(exp));
		} catch (Exception e) {
			log.error( "Exception. Reason: " + e.getMessage() );
			log.error(ExceptionUtils.getFullStackTrace(e));
			e.printStackTrace();
		}
		finally {
	        if (m != null) {
	        	m.finishProcessing();
	        }
		}
    	long finishTime = System.currentTimeMillis();
    	log.info("Processing Time: " + getElapsedTimeHoursMinutesSecondsString(finishTime - startTime));
	}
	 		
	private void processResults() throws Exception {
		int cnt = 0;
		if (threadResults.size() > 0) {
			for (Iterator<Future<ProcessingCounts>> it = threadResults.iterator();it.hasNext();) {
				Future<ProcessingCounts> res = it.next();
				if (res.isDone()) {
					ProcessingCounts c = res.get();
//					counts.incrementSuccess(c.getSuccessRecords());
					counts.incrementFailed(c.getFailedRecords());
					counts.incrementUpdatedRecords(c.getUpdatedRecords());
					counts.incrementInsertedRecords(c.getInsertedRecords());
					it.remove();
					System.out.println("Target: " + size + "\t" + counts);
					cnt++;
				}
			}
		}
		
		log.info("Processed " + cnt);
	}

	public static int saveErrors(final List<FileLoadDetail> errors, JdbcTemplate jdbcTemplate) {
		final Integer failCount[] = {0};
		jdbcTemplate = new StreamingJdbcTemplate(jdbcTemplate.getDataSource());
		int[] insMem = jdbcTemplate.batchUpdate(INSERT_INTAKE_ERROR_DETAIL, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				int i = 1;
				FileLoadDetail m = errors.get(idx);
				setParameter(i++, ps, m.getFileLoadSummaryId());
				setParameter(i++, ps, m.getRecordNumber());
				setParameter(i++, ps, m.getStatus());
				if (m.getStatus() == 1) {
					failCount[0] = failCount[0]+1;
				}
				setParameter(i++, ps, "Futurerx: " + m.getRemark());
			}
			@Override
			public int getBatchSize() {
				return errors.size();
			}
		});
		int recordsInserted = 0;
		for (int i: insMem) {
			recordsInserted += i;
		}
		errors.clear();
//		System.out.println("Errors / Warnings  Inserted: " + recordsInserted);
		return failCount[0];
	}
	
	public static void setParameter(Integer i, PreparedStatement ps, Object val) {
		try {
			if (val != null) {
				if (val.getClass().toString().contains("String") || val.getClass().toString().contains("Char")) {
					String strVal = (String) val;
					ps.setString(i, strVal);
				}
				else if (val.getClass().toString().contains("Long")) {
					Long lngVal = (Long) val;
					ps.setLong(i, lngVal);
				}
				else if (val.getClass().toString().contains("Boolean")) {
					Boolean blnVal = (Boolean) val;
					ps.setBoolean(i, blnVal);
				}
				else if (val.getClass().toString().contains("Int")) {
					Integer intVal = (Integer) val;
					ps.setInt(i, intVal);
				}
				else if (val.getClass().toString().contains("BigDecimal") || val.getClass().toString().contains("Float")) {
					BigDecimal intVal = (BigDecimal) val;
					ps.setBigDecimal(i, intVal);
				}
				else if (val.getClass().toString().contains("Date") || val.getClass().toString().contains("Time")) {
					Date intVal = (Date) val;
					ps.setDate(i, new java.sql.Date(intVal.getTime()));
				}
			}
			else {
				ps.setNull(i, Types.CHAR);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static String getElapsedTimeHoursMinutesSecondsString(long elapsedTime) {     
	    String format = String.format("%%0%dd", 2);
	    elapsedTime = elapsedTime / 1000;
	    String seconds = String.format(format, elapsedTime % 60);
	    String minutes = String.format(format, (elapsedTime % 3600) / 60);
	    String hours = String.format(format, elapsedTime / 3600);
	    String time =  hours + ":" + minutes + ":" + seconds;
	    return time;
	}

	public File getErrorFilesDirectory() {
		return errorFilesDirectory;
	}

	public void setErrorFilesDirectory(File errorFilesDirectory) {
		this.errorFilesDirectory = errorFilesDirectory;
	}

	public Integer getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Integer batchSize) {
		this.batchSize = batchSize;
	}

	public Integer getThreadPoolSize() {
		return threadPoolSize;
	}

	public void setThreadPoolSize(Integer threadPoolSize) {
		this.threadPoolSize = threadPoolSize;
	}

	public Boolean getTrackData() {
		return trackData;
	}

	public void setTrackData(Boolean trackData) {
		this.trackData = trackData;
	}


}

