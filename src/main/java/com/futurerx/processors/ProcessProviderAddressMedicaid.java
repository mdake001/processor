package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderAddressMedicaid;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderAddressMedicaid extends Processor {
    private static String PROVIDER_ADDR_MEDICAID_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_address_medicaid where loaderCompositeKey in (";
    private static String PROVIDER_ADDR_MEDICAID_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_address_medicaid(address_id,hcid,medicaid_id,state,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_ADDR_MEDICAID_UPDATE_QUERY = "UPDATE futurerx_provider.provider_address_medicaid SET address_id =?,hcid =?,medicaid_id =?,state =?,company_count =?,delta_date =?,provider_id =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =? Where id =?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderAddressMedicaid.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findAddressMedicaidIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_ADDR_MEDICAID_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderAddressMedicaidList(final List<FuturerxProviderAddressMedicaid> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDR_MEDICAID_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressMedicaid pam = records.get(idx);
				int i = 1;
				ps.setString(i++, pam.getAddressId());
				ps.setLong(i++, Long.parseLong(pam.getHcid()));
				ps.setString(i++, pam.getMedicaidId());
				ps.setString(i++, pam.getState());
				ps.setString(i++, pam.getCompanyCount());
				if(null != pam.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pam.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pam.getProviderId());
				ps.setString(i++, pam.getRecordType());
				ps.setString(i++, pam.getTierCode());
				ps.setString(i++, pam.getVerificationCode());
				if(null != pam.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pam.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pam.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderAddressMedicaidList(final List<FuturerxProviderAddressMedicaid> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDR_MEDICAID_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressMedicaid pam = records.get(idx);
				int i = 1;
				ps.setString(i++, pam.getAddressId());
				ps.setLong(i++, Long.parseLong(pam.getHcid()));
				ps.setString(i++, pam.getMedicaidId());
				ps.setString(i++, pam.getState());
				ps.setString(i++, pam.getCompanyCount());
				if(null != pam.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pam.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pam.getProviderId());
				ps.setString(i++, pam.getRecordType());
				ps.setString(i++, pam.getTierCode());
				ps.setString(i++, pam.getVerificationCode());
				if(null != pam.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pam.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pam.getLoaderCompositeKey());
	            ps.setLong(i++, pam.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderAddressMedicaid> insertRecordsList = new ArrayList<FuturerxProviderAddressMedicaid>();
		List<FuturerxProviderAddressMedicaid> updateRecordsList = new ArrayList<FuturerxProviderAddressMedicaid>();
		List<String> compositeKeyList = new ArrayList<String>();
	    List<String> hcidList = new ArrayList<String>();
	    List<String> addressList = new ArrayList<String>();
	    
		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderAddressMedicaid) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderAddressMedicaid) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderAddressMedicaid pa = (FuturerxProviderAddressMedicaid) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    	hcidList.add(pa.getHcid());
		    	addressList.add(pa.getAddressId());
		    }
		    else {
				rejectRecord = false;
		    }

		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> addressMedicaidIdMap = findAddressMedicaidIds(compositeKeyList);
			Map<String, Long> addressIdMap = ProcessProviderAddress.findAddressIds(addressList,"address_id");
			Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderAddressMedicaid pg = (FuturerxProviderAddressMedicaid) paObj;
				long id = 0;

				if (hcidMap.containsKey(pg.getHcid())) {
					pg.setHcid(hcidMap.get(pg.getHcid()).toString());

					if (addressIdMap.containsKey(pg.getAddressId())) {
						pg.setAddressId(addressIdMap.get(pg.getAddressId()).toString());

						if (addressMedicaidIdMap.containsKey(pg.getLoaderCompositeKey())) {
							id = addressMedicaidIdMap.get(pg.getLoaderCompositeKey());
						}
						if (id > 0) {
							pg.setId(id);
							updateRecordsList.add(pg);
						} else {
							insertRecordsList.add(pg);
						}
					}
				}
			}
		}
	    
		try {
	        updateProviderAddressMedicaidList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderAddressMedicaidList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider address medicaid Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
