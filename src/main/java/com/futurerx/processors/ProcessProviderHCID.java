package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderHCID;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderHCID extends Processor {
    private static String PROVIDER_HCID_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_hcid where loaderCompositeKey in (";
    private static String PROVIDER_HCID_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_hcid(hcid,delta_date,provider_id,record_type,loaderCompositeKey) VALUES(?,?,?,?,?)";
    private static String PROVIDER_HCID_UPDATE_QUERY = "UPDATE futurerx_provider.provider_hcid SET hcid =?,delta_date =?,provider_id =?,record_type =?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderHCID.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml"
			);
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findHCIDIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_HCID_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString(1), rs.getLong(2));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderHCIDList(final List<FuturerxProviderHCID> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_HCID_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderHCID ph = records.get(idx);
				int i = 1;
				ps.setString(i++, ph.getHcid());
				if(null != ph.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(ph.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, ph.getProviderId());
				ps.setString(i++, ph.getRecordType());
				ps.setString(i++, ph.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderHCIDList(final List<FuturerxProviderHCID> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_HCID_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderHCID ph = records.get(idx);
				int i = 1;
				ps.setString(i++, ph.getHcid());
				if(null != ph.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(ph.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, ph.getProviderId());
				ps.setString(i++, ph.getRecordType());
				ps.setString(i++, ph.getLoaderCompositeKey());
	            ps.setLong(i++, ph.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderHCID> insertRecordsList = new ArrayList<FuturerxProviderHCID>();
		List<FuturerxProviderHCID> updateRecordsList = new ArrayList<FuturerxProviderHCID>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderHCID) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderHCID) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderHCID pa = (FuturerxProviderHCID) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> hcidIdMap = findHCIDIds(compositeKeyList);

			for (Object paObj : records) {
				FuturerxProviderHCID ph = (FuturerxProviderHCID) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(ph.getLoaderCompositeKey())) {
					id = hcidIdMap.get(ph.getLoaderCompositeKey());
				}
				if (id > 0) {
					ph.setId(id);
					updateRecordsList.add(ph);
				} else {
					insertRecordsList.add(ph);
				}
			}
		}
	    
		try {
	        updateProviderHCIDList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderHCIDList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider HCID Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
