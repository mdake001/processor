package com.futurerx.processors;

import java.util.Date;
import java.util.List;

import com.futurerx.ProcessingCounts;

public abstract class Processor {
	abstract public ProcessingCounts processList(final List<?> records, final Date processingDate) throws Exception;
}
