package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderDegree;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderDegree extends Processor {
    private static String PROVIDER_DEGREE_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_degree where loaderCompositeKey in (";
    private static String PROVIDER_DEGREE_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_degree(hcid,company_count,delta_date,degree,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_DEGREE_UPDATE_QUERY = "UPDATE futurerx_provider.provider_degree SET hcid = ?,company_count = ?,delta_date = ?,degree = ?,provider_id = ?,record_type = ?,tier_code = ?,verification_code = ?,verification_date = ?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderDegree.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findDegreeIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_DEGREE_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderDegreeList(final List<FuturerxProviderDegree> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DEGREE_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDegree pd = records.get(idx);
				int i = 1;
				ps.setString(i++, pd.getHcid());
				ps.setString(i++, pd.getCompanyCount());
				if(null != pd.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pd.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getDegree());
				ps.setString(i++, pd.getProviderId());
				ps.setString(i++, pd.getRecordType());
				ps.setString(i++, pd.getTierCode());
				ps.setString(i++, pd.getVerificationCode());
				if(null != pd.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pd.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderDegreeList(final List<FuturerxProviderDegree> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DEGREE_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDegree pd = records.get(idx);
				int i = 1;
				ps.setString(i++, pd.getHcid());
				ps.setString(i++, pd.getCompanyCount());
				if(null != pd.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pd.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getDegree());
				ps.setString(i++, pd.getProviderId());
				ps.setString(i++, pd.getRecordType());
				ps.setString(i++, pd.getTierCode());
				ps.setString(i++, pd.getVerificationCode());
				if(null != pd.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pd.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getLoaderCompositeKey());
	            ps.setLong(i++, pd.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderDegree> insertRecordsList = new ArrayList<FuturerxProviderDegree>();
		List<FuturerxProviderDegree> updateRecordsList = new ArrayList<FuturerxProviderDegree>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderDegree) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderDegree) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderDegree pd = (FuturerxProviderDegree) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pd.getLoaderCompositeKey());
		    	hcidList.add(pd.getHcid());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pd.getDataIntakeFileId());
				fld.setRecordNumber(pd.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> degreeIdMap = findDegreeIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderDegree pd = (FuturerxProviderDegree) paObj;

				long id = 0;

				if (hcidIdMap.containsKey(pd.getHcid())) {
					pd.setHcid(hcidIdMap.get(pd.getHcid()).toString());

					if (degreeIdMap.containsKey(pd.getLoaderCompositeKey())) {
						id = degreeIdMap.get(pd.getLoaderCompositeKey());
					}
					if (id > 0) {
						pd.setId(id);
						updateRecordsList.add(pd);
					} else {
						insertRecordsList.add(pd);
					}
				}
			}
		}
	    
		try {
	        updateProviderDegreeList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderDegreeList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider degree Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }
}
