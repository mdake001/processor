package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderNPI;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderNPI extends Processor {
    private static String PROVIDER_NPI_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_npi where loaderCompositeKey in (";
    private static String PROVIDER_NPI_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_npi(hcid,company_count,deactivation_date,delta_date,npi,provider_id,npiRank,reactivation_date,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_NPI_UPDATE_QUERY = "UPDATE futurerx_provider.provider_npi SET hcid =?,company_count =?,deactivation_date =?,delta_date =?,npi =?,provider_id =?,npi_rank =?,reactivation_date =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderNPI.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml"
			);
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findNPIIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_NPI_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderNPIList(final List<FuturerxProviderNPI> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_NPI_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderNPI pn = records.get(idx);
				int i = 1;
				ps.setString(i++, pn.getHcid());
				ps.setString(i++, pn.getCompanyCount());
				if(null != pn.getDeactivationDate())
					ps.setDate(i++, new java.sql.Date(pn.getDeactivationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != pn.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pn.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getNpi());
				ps.setString(i++, pn.getProviderId());
				ps.setString(i++, pn.getNpiRank());
				if(null != pn.getReactivationDate())
					ps.setDate(i++, new java.sql.Date(pn.getReactivationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getRecordType());
				ps.setString(i++, pn.getTierCode());
				ps.setString(i++, pn.getVerificationCode());
				if(null != pn.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pn.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderNPIList(final List<FuturerxProviderNPI> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_NPI_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderNPI pn = records.get(idx);
				int i = 1;
				ps.setString(i++, pn.getHcid());
				ps.setString(i++, pn.getCompanyCount());
				if(null != pn.getDeactivationDate())
					ps.setDate(i++, new java.sql.Date(pn.getDeactivationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != pn.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pn.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getNpi());
				ps.setString(i++, pn.getProviderId());
				ps.setString(i++, pn.getNpiRank());
				if(null != pn.getReactivationDate())
					ps.setDate(i++, new java.sql.Date(pn.getReactivationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getRecordType());
				ps.setString(i++, pn.getTierCode());
				ps.setString(i++, pn.getVerificationCode());
				if(null != pn.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pn.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pn.getLoaderCompositeKey());
	            ps.setLong(i++, pn.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderNPI> insertRecordsList = new ArrayList<FuturerxProviderNPI>();
		List<FuturerxProviderNPI> updateRecordsList = new ArrayList<FuturerxProviderNPI>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderNPI) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderNPI) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderNPI pa = (FuturerxProviderNPI) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> npiIdMap = findNPIIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderNPI pn = (FuturerxProviderNPI) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(pn.getHcid())) {
					pn.setHcid(hcidIdMap.get(pn.getHcid()).toString());

					if (npiIdMap.containsKey(pn.getLoaderCompositeKey())) {
						id = npiIdMap.get(pn.getLoaderCompositeKey());
					}
					if (id > 0) {
						pn.setId(id);
						updateRecordsList.add(pn);
					} else {
						insertRecordsList.add(pn);
					}
				}
			}
		}
	    
		try {
	        updateProviderNPIList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderNPIList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider NPI Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
