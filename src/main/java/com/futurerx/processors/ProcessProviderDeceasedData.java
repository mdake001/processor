package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderDeceasedData;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderDeceasedData extends Processor {
    private static String PROVIDER_DECEASED_DATA_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_deceased_data where loaderCompositeKey in (";
    private static String PROVIDER_DECEASED_DATA_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_deceased_data(deceased_indicator,hcid,deceased_date,delta_date,provider_id,record_type,loaderCompositeKey) VALUES (?,?,?,?,?,?,?)";
    private static String PROVIDER_DECEASED_DATA_UPDATE_QUERY = "UPDATE futurerx_provider.provider_deceased_data SET deceased_indicator =?,hcid =?,deceased_date =?,delta_date =?,provider_id =?,record_type =?,loaderCompositeKey =? where id = ? ";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderDeceasedData.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findDeceasedDataIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_DECEASED_DATA_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderDeceasedDataList(final List<FuturerxProviderDeceasedData> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DECEASED_DATA_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDeceasedData pg = records.get(idx);
				int i = 1;
				ps.setString(i++, pg.getDeceasedIndicator());
				ps.setString(i++, pg.getHcid());
				if(null != pg.getDeceasedDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeceasedDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != pg.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getProviderId());
				ps.setString(i++, pg.getRecordType());
				ps.setString(i++, pg.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderDeceasedDataList(final List<FuturerxProviderDeceasedData> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DECEASED_DATA_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDeceasedData pg = records.get(idx);
				int i = 1;
				ps.setString(i++, pg.getDeceasedIndicator());
				ps.setString(i++, pg.getHcid());
				if(null != pg.getDeceasedDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeceasedDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != pg.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getProviderId());
				ps.setString(i++, pg.getRecordType());
				ps.setString(i++, pg.getLoaderCompositeKey());
	            ps.setLong(i++, pg.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderDeceasedData> insertRecordsList = new ArrayList<FuturerxProviderDeceasedData>();
		List<FuturerxProviderDeceasedData> updateRecordsList = new ArrayList<FuturerxProviderDeceasedData>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderDeceasedData) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderDeceasedData) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderDeceasedData pd = (FuturerxProviderDeceasedData) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pd.getHcid());
		    	compositeKeyList.add(pd.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pd.getDataIntakeFileId());
				fld.setRecordNumber(pd.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> deceasedDataIdMap = findDeceasedDataIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderDeceasedData pg = (FuturerxProviderDeceasedData) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(pg.getHcid())) {
					pg.setHcid(hcidIdMap.get(pg.getHcid()).toString());

					if (deceasedDataIdMap.containsKey(pg.getLoaderCompositeKey())) {
						id = deceasedDataIdMap.get(pg.getLoaderCompositeKey());
					}
					if (id > 0) {
						pg.setId(id);
						updateRecordsList.add(pg);
					} else {
						insertRecordsList.add(pg);
					}
				}
			}
		}
	    
		try {
	        updateProviderDeceasedDataList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderDeceasedDataList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider deceasedData Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
