 package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderAddress;
import com.futurerx.utils.FileLoadDetail;


public class ProcessProviderAddress extends Processor {
    private static final String PROVIDER_ADDRESS_SEARCH_LIST_QUERY = "SELECT #searchField, id from futurerx_provider.provider_address where #searchField in (";
    private static final String PROVIDER_ADDRESS_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_address(address_id,hcid,address1,address2,city,company_count,county,deltaDate,dpv,geoReturn,latitude,longitude,provider_id,addrRank,recordType,state,tierCode,verificationCode,verificationDate,zip,zip4,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String PROVIDER_ADDRESS_UPDATE_QUERY = "UPDATE futurerx_provider.provider_address SET address_id = ?,hcid = ?,address1 = ?,address2 = ?,city = ?,company_count = ?,county = ?,deltaDate = ?,dpv = ?,geoReturn = ?,latitude = ?,longitude = ?,provider_id = ?,addrRank = ?,recordType = ?,state = ?,tierCode = ?,verificationCode = ?,verificationDate = ?,zip = ?,zip4 = ?,loaderCompositeKey = ? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderAddress.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    private static void insertProviderAddressList(final List<FuturerxProviderAddress> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddress pa = records.get(idx);
				int i = 1;
				ps.setString(i++, pa.getAddressId());
				ps.setString(i++, pa.getHcid());
				ps.setString(i++, pa.getAddress1());
				ps.setString(i++, pa.getAddress2());
				ps.setString(i++, pa.getCity());
				ps.setString(i++, pa.getCompanyCount());
				ps.setString(i++, pa.getCounty());
				if(null != pa.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pa.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pa.getDpv());
				ps.setString(i++, pa.getGeoReturn());
				ps.setString(i++, pa.getLatitude());
				ps.setString(i++, pa.getLongitude());
				ps.setString(i++, pa.getProviderId());
				ps.setString(i++, pa.getAddrRank());
				ps.setString(i++, pa.getRecordType());
				ps.setString(i++, pa.getState());
				ps.setString(i++, pa.getTierCode());
				ps.setString(i++, pa.getVerificationCode());
				if(null != pa.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pa.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pa.getZip());
				ps.setString(i++, pa.getZip4());
				ps.setString(i++, pa.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderAddressList(final List<FuturerxProviderAddress> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddress pa = records.get(idx);
				int i = 1;
				ps.setString(i++, pa.getAddressId());
				ps.setLong(i++, Long.parseLong(pa.getHcid()));
				ps.setString(i++, pa.getAddress1());
				ps.setString(i++, pa.getAddress2());
				ps.setString(i++, pa.getCity());
				ps.setString(i++, pa.getCompanyCount());
				ps.setString(i++, pa.getCounty());
				if(null != pa.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pa.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pa.getDpv());
				ps.setString(i++, pa.getGeoReturn());
				ps.setString(i++, pa.getLatitude());
				ps.setString(i++, pa.getLongitude());
				ps.setString(i++, pa.getProviderId());
				ps.setString(i++, pa.getAddrRank());
				ps.setString(i++, pa.getRecordType());
				ps.setString(i++, pa.getState());
				ps.setString(i++, pa.getTierCode());
				ps.setString(i++, pa.getVerificationCode());
				if(null != pa.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pa.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pa.getZip());
				ps.setString(i++, pa.getZip4());
				ps.setString(i++, pa.getLoaderCompositeKey());
	            
	            ps.setLong(i++, pa.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderAddress> insertRecordsList = new ArrayList<FuturerxProviderAddress>();
		List<FuturerxProviderAddress> updateRecordsList = new ArrayList<FuturerxProviderAddress>();
		List<String> compositeKeyList = new ArrayList<String>();
		List<String> hcidList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderAddress) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderAddress) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderAddress pa = (FuturerxProviderAddress) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    	hcidList.add(pa.getHcid());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> addressIdMap = findAddressIds(compositeKeyList);
			Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);
			
			for (Object paObj : records) {
				FuturerxProviderAddress pa = (FuturerxProviderAddress) paObj;
				long id = 0;

				if (hcidMap.containsKey(pa.getHcid())) {
					pa.setHcid(hcidMap.get(pa.getHcid()).toString());

					if (addressIdMap.containsKey(pa.getLoaderCompositeKey())) {
						id = addressIdMap.get(pa.getLoaderCompositeKey());
					}
					if (id > 0) {
						pa.setId(id);
						updateRecordsList.add(pa);
					} else {
						insertRecordsList.add(pa);
					}
					if (remark.length() > 0) {
						FileLoadDetail fld = new FileLoadDetail();
						fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
						fld.setRecordNumber(pa.getRecordNumber().longValue());
						fld.setRemark(remark);
						fld.setStatus((fld.getRemark().contains("FAIL") ? 1 : 2));
						errorList.add(fld);
						remark = new String();
					}
				}

			}
		}
	    
		try {
	        updateProviderAddressList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderAddressList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider Address Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }
    
    public static Map<String, Long> findAddressIds(final List<String> records, String searchField) {
		final Map<String, Long> map = new HashMap<String, Long>();

		final StringBuilder queryBuilder = new StringBuilder(PROVIDER_ADDRESS_SEARCH_LIST_QUERY.replaceAll("#searchField", searchField));

    	for (int i = 0;i< records.size();i++) {
    		
    		if (i < records.size()-1) {
    			queryBuilder.append("?,");
    		} else {
    			queryBuilder.append("?)");    			
    		}
    	}
    	
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: records) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString(1), rs.getLong(2));					
				}
			}  
		);
    	return map;			
	}
    
	public static Map<String, Long> findAddressIds(final List<String> records) {
    	String searchField = "loaderCompositeKey";
    	return findAddressIds(records, searchField);
	}

}
