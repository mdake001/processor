package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderAddressPhone;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderAddressPhone extends Processor {
    private static String PROVIDER_ADDRESS_PHONE_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_address_phone where loaderCompositeKey in (";
    private static String PROVIDER_ADDRESS_PHONE_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_address_phone(address_id,hcid,phone_number,phone_type,company_count,delta_date,provider_id,addrPhoneRank,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_ADDRESS_PHONE_UPDATE_QUERY = "UPDATE futurerx_provider.provider_address_phone SET (address_id =?,hcid =?,phone_number =?,phone_type =?,company_count =?,delta_date =?,provider_id =?,addrPhoneRank =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =?)";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderAddressPhone.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findAddressPhoneIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_ADDRESS_PHONE_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderAddressPhoneList(final List<FuturerxProviderAddressPhone> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_PHONE_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressPhone pap = records.get(idx);
				int i = 1;
				ps.setLong(i++, Long.parseLong(pap.getAddressId()));
				ps.setLong(i++, Long.parseLong(pap.getHcid()));
				ps.setString(i++, pap.getPhoneNumber());
				ps.setString(i++, pap.getPhoneType());
				ps.setString(i++, pap.getCompanyCount());
				if(null != pap.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pap.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pap.getProviderId());
				ps.setString(i++, pap.getAddrPhoneRank());
				ps.setString(i++, pap.getRecordType());
				ps.setString(i++, pap.getTierCode());
				ps.setString(i++, pap.getVerificationCode());
				if(null != pap.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pap.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pap.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderAddressPhoneList(final List<FuturerxProviderAddressPhone> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_PHONE_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressPhone pap = records.get(idx);
				int i = 1;
				ps.setLong(i++, Long.parseLong(pap.getAddressId()));
				ps.setLong(i++, Long.parseLong(pap.getHcid()));
				ps.setString(i++, pap.getPhoneNumber());
				ps.setString(i++, pap.getPhoneType());
				ps.setString(i++, pap.getCompanyCount());
				if(null != pap.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pap.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pap.getProviderId());
				ps.setString(i++, pap.getAddrPhoneRank());
				ps.setString(i++, pap.getRecordType());
				ps.setString(i++, pap.getTierCode());
				ps.setString(i++, pap.getVerificationCode());
				if(null != pap.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pap.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pap.getLoaderCompositeKey());
				ps.setLong(i++, pap.getId());
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderAddressPhone> insertRecordsList = new ArrayList<FuturerxProviderAddressPhone>();
		List<FuturerxProviderAddressPhone> updateRecordsList = new ArrayList<FuturerxProviderAddressPhone>();
		List<String> compositeKeyList = new ArrayList<String>();
		List<String> hcidList = new ArrayList<String>();
		List<String> addressList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderAddressPhone) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderAddressPhone) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderAddressPhone pa = (FuturerxProviderAddressPhone) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    	hcidList.add(pa.getHcid());
		    	addressList.add(pa.getAddressId());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if(compositeKeyList.size() > 0) {
			Map<String, Long> addressPhoneIdMap = findAddressPhoneIds(compositeKeyList);
			Map<String, Long> addressIdMap = ProcessProviderAddress.findAddressIds(addressList,"address_id");
			Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);
		    
	    for (Object paObj: records) {
	    	FuturerxProviderAddressPhone pg = (FuturerxProviderAddressPhone) paObj;
	    	long id = 0;	
			if (hcidMap.containsKey(pg.getHcid())) {
				pg.setHcid(hcidMap.get(pg.getHcid()).toString());
				if (addressIdMap.containsKey(pg.getAddressId())) {
					pg.setAddressId(addressIdMap.get(pg.getAddressId()).toString());

					if (addressPhoneIdMap.containsKey(pg.getLoaderCompositeKey())) {
						id = addressPhoneIdMap.get(pg.getLoaderCompositeKey());
					}
					if (id > 0) {
						pg.setId(id);
						updateRecordsList.add(pg);
					} else {
						insertRecordsList.add(pg);
					}
				}
			}
	       }
		}
	    
		try {
	        updateProviderAddressPhoneList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderAddressPhoneList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider addressPhone Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
