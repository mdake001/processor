package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderAddressSPI;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderAddressSPI extends Processor {
    private static String PROVIDER_ADDRESS_SPI_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_address_spi where loaderCompositeKey in (";
    private static String PROVIDER_ADDRESS_SPI_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_address_spi(address_id,hcid,active_end_date,cancel,ccr,census,spi_change,company_count,delta_date,eligibility,med_history,spi_new,provider_id,re_supp,record_type,refill,rx_fill,service_level10,service_level11,service_level12,service_level13,service_level14,service_level15,service_level_code,spi_location_code,spi_root,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_ADDRESS_SPI_UPDATE_QUERY = "UPDATE futurerx_provider.provider_address_spi SET (address_id =?,hcid =?,active_end_date =?,cancel =?,ccr =?,census =?,spi_change =?,company_count =?,delta_date =?,eligibility =?,med_history =?,npi_new =?,provider_id =?,re_supp =?,record_type =?,refill =?,rx_fill =?,service_level10 =?,service_level11 =?,service_level12 =?,service_level13 =?,service_level14 =?,service_level15 =?,service_level_code =?,spi_location_code =?,spi_root =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =?)";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderAddressSPI.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findAddressSPIIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_ADDRESS_SPI_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderAddressSPIList(final List<FuturerxProviderAddressSPI> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_SPI_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressSPI pas = records.get(idx);
				int i = 1;
				ps.setString(i++, pas.getAddressId());
				ps.setString(i++, pas.getHcid());
				if(null != pas.getActiveEndDate())
					ps.setDate(i++, new java.sql.Date(pas.getActiveEndDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getCancel());
				ps.setString(i++, pas.getCcr());
				if(null != pas.getCensus())
					ps.setDate(i++, new java.sql.Date(pas.getCensus().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getChange());
				ps.setString(i++, pas.getCompanyCount());
				if(null != pas.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pas.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getEligibility());
				ps.setString(i++, pas.getMedHistory());
				ps.setString(i++, pas.getNpiNew());
				ps.setString(i++, pas.getProviderId());
				ps.setString(i++, pas.getReSupp());
				ps.setString(i++, pas.getRecordType());
				ps.setString(i++, pas.getRefill());
				ps.setString(i++, pas.getRxFill());
				ps.setString(i++, pas.getServiceLevel10());
				ps.setString(i++, pas.getServiceLevel11());
				ps.setString(i++, pas.getServiceLevel12());
				ps.setString(i++, pas.getServiceLevel13());
				ps.setString(i++, pas.getServiceLevel14());
				ps.setString(i++, pas.getServiceLevel15());
				ps.setString(i++, pas.getServiceLevelCode());
				ps.setString(i++, pas.getSpiLocationCode());
				ps.setString(i++, pas.getSpiRoot());
				ps.setString(i++, pas.getTierCode());
				ps.setString(i++, pas.getVerificationCode());
				if(null != pas.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pas.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderAddressSPIList(final List<FuturerxProviderAddressSPI> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_ADDRESS_SPI_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderAddressSPI pas = records.get(idx);
				int i = 1;
				ps.setString(i++, pas.getAddressId());
				ps.setString(i++, pas.getHcid());
				if(null != pas.getActiveEndDate())
					ps.setDate(i++, new java.sql.Date(pas.getActiveEndDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getCancel());
				ps.setString(i++, pas.getCcr());
				if(null != pas.getCensus())
					ps.setDate(i++, new java.sql.Date(pas.getCensus().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getChange());
				ps.setString(i++, pas.getCompanyCount());
				if(null != pas.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pas.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getEligibility());
				ps.setString(i++, pas.getMedHistory());
				ps.setString(i++, pas.getNpiNew());
				ps.setString(i++, pas.getProviderId());
				ps.setString(i++, pas.getReSupp());
				ps.setString(i++, pas.getRecordType());
				ps.setString(i++, pas.getRefill());
				ps.setString(i++, pas.getRxFill());
				ps.setString(i++, pas.getServiceLevel10());
				ps.setString(i++, pas.getServiceLevel11());
				ps.setString(i++, pas.getServiceLevel12());
				ps.setString(i++, pas.getServiceLevel13());
				ps.setString(i++, pas.getServiceLevel14());
				ps.setString(i++, pas.getServiceLevel15());
				ps.setString(i++, pas.getServiceLevelCode());
				ps.setString(i++, pas.getSpiLocationCode());
				ps.setString(i++, pas.getSpiRoot());
				ps.setString(i++, pas.getTierCode());
				ps.setString(i++, pas.getVerificationCode());
				if(null != pas.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pas.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pas.getLoaderCompositeKey());
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderAddressSPI> insertRecordsList = new ArrayList<FuturerxProviderAddressSPI>();
		List<FuturerxProviderAddressSPI> updateRecordsList = new ArrayList<FuturerxProviderAddressSPI>();
		List<String> compositeKeyList = new ArrayList<String>();
		List<String> hcidList = new ArrayList<String>();
		List<String> addressList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderAddressSPI) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderAddressSPI) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderAddressSPI pa = (FuturerxProviderAddressSPI) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    	hcidList.add(pa.getHcid());
		    	addressList.add(pa.getAddressId());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> addressSPIIdMap = findAddressSPIIds(compositeKeyList);
			Map<String, Long> addressIdMap = ProcessProviderAddress.findAddressIds(addressList, "address_id");
			Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderAddressSPI pg = (FuturerxProviderAddressSPI) paObj;
				long id = 0;

				if (hcidMap.containsKey(pg.getHcid())) {
					pg.setHcid(hcidMap.get(pg.getHcid()).toString());

					if (addressIdMap.containsKey(pg.getAddressId())) {
						pg.setAddressId(addressIdMap.get(pg.getAddressId()).toString());

						if (addressSPIIdMap.containsKey(pg.getLoaderCompositeKey())) {
							id = addressSPIIdMap.get(pg.getLoaderCompositeKey());
						}
						if (id > 0) {
							pg.setId(id);
							updateRecordsList.add(pg);
						} else {
							insertRecordsList.add(pg);
						}
					}

				}
			}
		}
	    
		try {
	        updateProviderAddressSPIList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderAddressSPIList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider AddressSPI Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
