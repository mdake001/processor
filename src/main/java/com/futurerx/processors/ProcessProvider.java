package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProvider;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProvider extends Processor {
	private static final String FIND_BY_EXT_ID_LIST_QUERY = "select #searchField, id from futurerx_provider.provider where #searchField in (";
	private static final String INSERT_PROVIDER_QUERY = "INSERT INTO futurerx_provider.provider(hcid,companyCount,deltaDate,providerId,recordType,providerType,loaderCompositeKey)VALUES(?,?,?,?,?,?,?)";
	private static final String UPDATE_PROVIDER_QUERY = "update futurerx_provider.provider set hcid = ?, companyCount=?,deltaDate=?,providerId=?,recordType=?,providerType=?,loaderCompositeKey=? where id = ?";

	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

	private static Logger log = LoggerFactory.getLogger(ProcessProvider.class);
	

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		ProcessingCounts c = new ProcessingCounts();
    	List<FuturerxProvider> updateList = new ArrayList<FuturerxProvider>();
    	List<FuturerxProvider> insertList = new ArrayList<FuturerxProvider>();
    	List<String> compositeKeyList = new ArrayList<String>();
    	List<String> hcidList = new ArrayList<String>();

    	String remark = new String();
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProvider) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProvider) records.get(0)).getRecordNumber().longValue();
		
		Map<String, FuturerxProvider> providerMap = new HashMap<String, FuturerxProvider>();
		for (Object obj: records) {
			FuturerxProvider p = (FuturerxProvider)obj;
			providerMap.put(p.getLoaderCompositeKey(), p);
		}
		for (FuturerxProvider obj: providerMap.values()) {
			FuturerxProvider rec = (FuturerxProvider) obj;
			log.debug("Provider: " + rec.getProviderId());
			
	    	if (!compositeKeyList.contains(rec.getLoaderCompositeKey())) {
	    		compositeKeyList.add(rec.getLoaderCompositeKey());
	    	} else {
	    		log.warn("Duplicate record. Provider ID: " + rec.getProviderId());
				 remark += "WARN: " + "Duplicate record. Provider ID: " + rec.getProviderId();
	    	}
	    	
	    	if (!hcidList.contains(rec.getLoaderCompositeKey())) {
	    		hcidList.add(rec.getLoaderCompositeKey());
	    	} 
	    		    	
	    	if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(rec.getDataIntakeFileId());
				fld.setRecordNumber(rec.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);
		Map<String, Long> idMap = findProviderIds(compositeKeyList);
		for (Object obj: records) {
			FuturerxProvider p = (FuturerxProvider) obj;
		
			long providerId = 0;	
			
			if (hcidMap.containsKey(p.getHcid())){
				p.setHcid(hcidMap.get(p.getHcid()).toString());
				if (idMap.containsKey(p.getLoaderCompositeKey())) {
					providerId = idMap.get(p.getLoaderCompositeKey());
				} 				
				if (providerId  > 0) {
					p.setId(providerId);
					updateList.add(p);
				} else {
					insertList.add(p);
				}
			}
		}
		try {
			updateProviderList(updateList, processingDateTime);
			c.incrementUpdatedRecords(updateList.size());
			insertProviderList(insertList, processingDateTime);    	
			c.incrementInsertedRecords(insertList.size());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Provider Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
	}

    private static void insertProviderList(final List<FuturerxProvider> records, final Date processingDateTime) {
    	if (records.size() > 0) {
	    	jdbcTemplate.batchUpdate(INSERT_PROVIDER_QUERY, new BatchPreparedStatementSetter() {
				
				public void setValues(PreparedStatement ps, int i) throws SQLException {
		            int idx = 1;
		            FuturerxProvider p = records.get(i);
		            ps.setLong(idx++, Long.parseLong(p.getHcid()));
		            ps.setString(idx++, p.getCompanyCount());
		            ps.setDate(idx++, new java.sql.Date(p.getDeltaDate().getTime()));
		            ps.setLong(idx++, p.getProviderId());
		        	ps.setString(idx++, p.getRecordType());
		            ps.setString(idx++, p.getProviderType());
		            ps.setString(idx++, p.getLoaderCompositeKey());
					}
				public int getBatchSize() {
					return records.size();
				}
			});
    	}
	}

    private static void updateProviderList(final List<FuturerxProvider> records, final Date processingDateTime) {
    	if (records.size() > 0) {
	    	jdbcTemplate.batchUpdate(UPDATE_PROVIDER_QUERY, new BatchPreparedStatementSetter() {
				
				public void setValues(PreparedStatement ps, int i) throws SQLException {
		            FuturerxProvider p = records.get(i);
					int idx = 1;
					ps.setLong(idx++, Long.parseLong(p.getHcid()));
		            ps.setString(idx++, p.getCompanyCount());
		            if(null != p.getDeltaDate())
		            	ps.setDate(idx++, new java.sql.Date(p.getDeltaDate().getTime()));
		            else
		            	ps.setNull(idx++, Types.DATE);
		       		ps.setLong(idx++, p.getProviderId());
		            ps.setString(idx++, p.getRecordType());
		            ps.setString(idx++, p.getProviderType());
		            ps.setString(idx++, p.getLoaderCompositeKey());
		            ps.setLong(idx++, p.getId()); 
		           }
				
				public int getBatchSize() {				
					return records.size();
				}
			});
    	}
	}
	
	public static Map<String, Long> findProviderIds(final List<String> records, String searchField) {
		final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(FIND_BY_EXT_ID_LIST_QUERY.replaceAll("#searchField", searchField));

    	for (int i = 0;i< records.size();i++) {
    		
    		if (i < records.size()-1) {
    			queryBuilder.append("?,");
    		} else {
    			queryBuilder.append("?)");    			
    		}
    	}
    	
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: records) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString(1), rs.getLong(2));					
				}
			}  
		);
    	return map;			
	}
	
	public static Map<String, Long> findProviderIds(final List<String> records) {
    	String searchField = "loaderCompositeKey";
    	return findProviderIds(records, searchField);
	}
	
}
