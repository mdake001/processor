package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderBirthDate;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderBirthDate extends Processor {
    private static String PROVIDER_BIRTH_DATE_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_birth_date where loaderCompositeKey in (";
    private static String PROVIDER_BIRTH_DATE_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_birth_date(hcid,birth_date,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_BIRTH_DATE_UPDATE_QUERY = "UPDATE futurerx_provider.provider_birth_date SET (hcid =?,birth_date =?,company_count =?,delta_date =?,provider_id =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =?)";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderBirthDate.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findBirthDateIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_BIRTH_DATE_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderBirthDateList(final List<FuturerxProviderBirthDate> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_BIRTH_DATE_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderBirthDate pb = records.get(idx);
				int i = 1;
				ps.setString(i++, pb.getHcid());
				if(null != pb.getBirthDate())
					ps.setDate(i++, new java.sql.Date(pb.getBirthDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getCompanyCount());
				if(null != pb.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pb.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getProviderId());
				ps.setString(i++, pb.getRecordType());
				ps.setString(i++, pb.getTierCode());
				ps.setString(i++, pb.getVerificationCode());
				if(null != pb.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pb.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderBirthDateList(final List<FuturerxProviderBirthDate> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_BIRTH_DATE_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderBirthDate pb = records.get(idx);
				int i = 1;
				ps.setString(i++, pb.getHcid());
				if(null != pb.getBirthDate())
					ps.setDate(i++, new java.sql.Date(pb.getBirthDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getCompanyCount());
				if(null != pb.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pb.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getProviderId());
				ps.setString(i++, pb.getRecordType());
				ps.setString(i++, pb.getTierCode());
				ps.setString(i++, pb.getVerificationCode());
				if(null != pb.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pb.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pb.getLoaderCompositeKey());
	            ps.setLong(i++, pb.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderBirthDate> insertRecordsList = new ArrayList<FuturerxProviderBirthDate>();
		List<FuturerxProviderBirthDate> updateRecordsList = new ArrayList<FuturerxProviderBirthDate>();
		List<String> compositeKeyList = new ArrayList<String>();
		List<String> hcidList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderBirthDate) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderBirthDate) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderBirthDate pb = (FuturerxProviderBirthDate) srcObj;
		    if (!rejectRecord) {
		    	compositeKeyList.add(pb.getLoaderCompositeKey());
		    	hcidList.add(pb.getHcid());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pb.getDataIntakeFileId());
				fld.setRecordNumber(pb.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> birthDateIdMap = findBirthDateIds(compositeKeyList);
			Map<String, Long> hcidMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderBirthDate pg = (FuturerxProviderBirthDate) paObj;
				long id = 0;

				if (hcidMap.containsKey(pg.getHcid())) {
					pg.setHcid(hcidMap.get(pg.getHcid()).toString());

					if (birthDateIdMap.containsKey(pg.getLoaderCompositeKey())) {
						id = birthDateIdMap.get(pg.getLoaderCompositeKey());
					}
					if (id > 0) {
						pg.setId(id);
						updateRecordsList.add(pg);
					} else {
						insertRecordsList.add(pg);
					}
				}
			}
		}
	    
		try {
	        updateProviderBirthDateList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderBirthDateList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider BirthDate Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
