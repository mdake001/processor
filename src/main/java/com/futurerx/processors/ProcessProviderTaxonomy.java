package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderTaxonomy;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderTaxonomy extends Processor {
    private static String PROVIDER_TAXONOMY_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_taxonomy where loaderCompositeKey in (";
    private static String PROVIDER_TAXONOMY_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_taxonomy(hcid,taxonomy_code,company_count,delta_date,primary_taxonomy_indicator,provider_id,record_type,taxonomy_description,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_TAXONOMY_UPDATE_QUERY = "UPDATE futurerx_provider.provider_taxonomy SET hcid =?,taxonomy_code =?,company_count =?,delta_date =?,primary_taxonomy_indicator =?,provider_id =?,record_type =?,taxonomy_description =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderTaxonomy.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findTaxonomyIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_TAXONOMY_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderTaxonomyList(final List<FuturerxProviderTaxonomy> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_TAXONOMY_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderTaxonomy pt = records.get(idx);
				int i = 1;
				ps.setString(i++, pt.getHcid());
				ps.setString(i++, pt.getTaxonomyCode());
				ps.setString(i++, pt.getCompanyCount());
				if(null != pt.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pt.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pt.getPrimaryTaxonomyIndicator());
				ps.setString(i++, pt.getProviderId());
				ps.setString(i++, pt.getRecordType());
				ps.setString(i++, pt.getTaxonomyDescription());
				ps.setString(i++, pt.getTierCode());
				ps.setString(i++, pt.getVerificationCode());
				if(null != pt.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pt.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pt.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderTaxonomyList(final List<FuturerxProviderTaxonomy> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_TAXONOMY_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderTaxonomy pt = records.get(idx);
				int i = 1;
				ps.setString(i++, pt.getHcid());
				ps.setString(i++, pt.getTaxonomyCode());
				ps.setString(i++, pt.getCompanyCount());
				if(null != pt.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pt.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pt.getPrimaryTaxonomyIndicator());
				ps.setString(i++, pt.getProviderId());
				ps.setString(i++, pt.getRecordType());
				ps.setString(i++, pt.getTaxonomyDescription());
				ps.setString(i++, pt.getTierCode());
				ps.setString(i++, pt.getVerificationCode());
				if(null != pt.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pt.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pt.getLoaderCompositeKey());
	            ps.setLong(i++, pt.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderTaxonomy> insertRecordsList = new ArrayList<FuturerxProviderTaxonomy>();
		List<FuturerxProviderTaxonomy> updateRecordsList = new ArrayList<FuturerxProviderTaxonomy>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderTaxonomy) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderTaxonomy) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderTaxonomy pa = (FuturerxProviderTaxonomy) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> taxonomyIdMap = findTaxonomyIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderTaxonomy pt = (FuturerxProviderTaxonomy) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(pt.getHcid())) {
					pt.setHcid(hcidIdMap.get(pt.getHcid()).toString());

					if (taxonomyIdMap.containsKey(pt.getLoaderCompositeKey())) {
						id = taxonomyIdMap.get(pt.getLoaderCompositeKey());
					}
					if (id > 0) {
						pt.setId(id);
						updateRecordsList.add(pt);
					} else {
						insertRecordsList.add(pt);
					}
				}
			}
		}
	    
		try {
	        updateProviderTaxonomyList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderTaxonomyList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider taxonomy Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
