package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderDEANumber;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderDeaNumber extends Processor {
    private static String PROVIDER_DEA_NUMBER_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_dea_number where loaderCompositeKey in (";
    private static String PROVIDER_DEA_NUMBER_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_dea_number(dea_number,hcid,company_count,dea_status_code,delta_date,drug_schedule,provider_id,record_type,renewal_date,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_DEA_NUMBER_UPDATE_QUERY = "UPDATE futurerx_provider.provider_dea_number SET (dea_number =?,hcid =?,company_count =?,dea_status_code =?,delta_date =?,drug_schedule =?,provider_id =?,record_type =?,renewal_date =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =?) ";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderDeaNumber.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findDeaNumberIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_DEA_NUMBER_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderDeaNumberList(final List<FuturerxProviderDEANumber> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DEA_NUMBER_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDEANumber pd = records.get(idx);
				int i = 1;
				ps.setString(i++, pd.getDeaNumber());
				ps.setString(i++, pd.getHcid());
				ps.setString(i++, pd.getCompanyCount());
				ps.setString(i++, pd.getDeaStatusCode());
				if(null != pd.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pd.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getDrugSchedule());
				ps.setString(i++, pd.getProviderId());
				ps.setString(i++, pd.getRecordType());
				if(null != pd.getRenewalDate())
					ps.setDate(i++, new java.sql.Date(pd.getRenewalDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getTierCode());
				ps.setString(i++, pd.getVerificationCode());
				if(null != pd.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pd.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderDeaNumberList(final List<FuturerxProviderDEANumber> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_DEA_NUMBER_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderDEANumber pd = records.get(idx);
				int i = 1;
				ps.setString(i++, pd.getDeaNumber());
				ps.setString(i++, pd.getHcid());
				ps.setString(i++, pd.getCompanyCount());
				ps.setString(i++, pd.getDeaStatusCode());
				if(null != pd.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pd.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getDrugSchedule());
				ps.setString(i++, pd.getProviderId());
				ps.setString(i++, pd.getRecordType());
				if(null != pd.getRenewalDate())
					ps.setDate(i++, new java.sql.Date(pd.getRenewalDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getTierCode());
				ps.setString(i++, pd.getVerificationCode());
				if(null != pd.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pd.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pd.getLoaderCompositeKey());
	            ps.setLong(i++, pd.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderDEANumber> insertRecordsList = new ArrayList<FuturerxProviderDEANumber>();
		List<FuturerxProviderDEANumber> updateRecordsList = new ArrayList<FuturerxProviderDEANumber>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderDEANumber) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderDEANumber) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderDEANumber pa = (FuturerxProviderDEANumber) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> deaNumberIdMap = findDeaNumberIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderDEANumber pd = (FuturerxProviderDEANumber) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(pd.getHcid())) {
					pd.setHcid(hcidIdMap.get(pd.getHcid()).toString());

					if (deaNumberIdMap.containsKey(pd.getLoaderCompositeKey())) {
						id = deaNumberIdMap.get(pd.getLoaderCompositeKey());
					}
					if (id > 0) {
						pd.setId(id);
						updateRecordsList.add(pd);
					} else {
						insertRecordsList.add(pd);
					}
				}
			}
		}
	    
		try {
	        updateProviderDeaNumberList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderDeaNumberList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider DEA Number Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
