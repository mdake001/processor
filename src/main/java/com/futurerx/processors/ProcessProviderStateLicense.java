package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderStateLicense;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderStateLicense extends Processor {
    private static String PROVIDER_STATE_LICENSE_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_state_license where loaderCompositeKey in (";
    private static String PROVIDER_STATE_LICENSE_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_state_license(hcid,license_number,license_state,company_count,delta_date,license_expiration_date,license_issue_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_STATE_LICENSE_UPDATE_QUERY = "UPDATE futurerx_provider.provider_state_license SET (hcid =?,license_number =?,license_state =?,company_count =?,delta_date =?,license_expiration_date =?,license_issue_date =?,provider_id =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =?)  WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderStateLicense.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findStateLicenseIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_STATE_LICENSE_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderStateLicenseList(final List<FuturerxProviderStateLicense> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_STATE_LICENSE_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderStateLicense psl = records.get(idx);
				int i = 1;
				ps.setString(i++, psl.getHcid());
				ps.setString(i++, psl.getLicenseNumber());
				ps.setString(i++, psl.getLicenseState());
				ps.setString(i++, psl.getCompanyCount());
				if(null != psl.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(psl.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != psl.getLicenseExpirationDate())
					ps.setDate(i++, new java.sql.Date(psl.getLicenseExpirationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != psl.getLicenseIssueDate())
					ps.setDate(i++, new java.sql.Date(psl.getLicenseIssueDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, psl.getProviderId());
				ps.setString(i++, psl.getRecordType());
				ps.setString(i++, psl.getTierCode());
				ps.setString(i++, psl.getVerificationCode());
				if(null != psl.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(psl.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, psl.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderStateLicenseList(final List<FuturerxProviderStateLicense> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_STATE_LICENSE_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderStateLicense psl = records.get(idx);
				int i = 1;
				ps.setString(i++, psl.getHcid());
				ps.setString(i++, psl.getLicenseNumber());
				ps.setString(i++, psl.getLicenseState());
				ps.setString(i++, psl.getCompanyCount());
				if(null != psl.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(psl.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != psl.getLicenseExpirationDate())
					ps.setDate(i++, new java.sql.Date(psl.getLicenseExpirationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				if(null != psl.getLicenseIssueDate())
					ps.setDate(i++, new java.sql.Date(psl.getLicenseIssueDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, psl.getProviderId());
				ps.setString(i++, psl.getRecordType());
				ps.setString(i++, psl.getTierCode());
				ps.setString(i++, psl.getVerificationCode());
				if(null != psl.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(psl.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, psl.getLoaderCompositeKey());
	            ps.setLong(i++, psl.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderStateLicense> insertRecordsList = new ArrayList<FuturerxProviderStateLicense>();
		List<FuturerxProviderStateLicense> updateRecordsList = new ArrayList<FuturerxProviderStateLicense>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderStateLicense) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderStateLicense) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderStateLicense pa = (FuturerxProviderStateLicense) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if(compositeKeyList.size() > 0) {
			Map<String, Long> stateLicenseIdMap = findStateLicenseIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);
		    
	    for (Object paObj: records) {
	    	FuturerxProviderStateLicense ps = (FuturerxProviderStateLicense) paObj;
	    	long id = 0;	
	    	
	    	if (hcidIdMap.containsKey(ps.getHcid())) {
				ps.setHcid(hcidIdMap.get(ps.getHcid()).toString());
				
				if (stateLicenseIdMap.containsKey(ps.getLoaderCompositeKey())) {
					id = stateLicenseIdMap.get(ps.getLoaderCompositeKey());
				}
				if (id > 0) {
					ps.setId(id);
					updateRecordsList.add(ps);
				} else {
					insertRecordsList.add(ps);
				}
			}
	       }
		}
	    
		try {
	        updateProviderStateLicenseList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderStateLicenseList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider stateLicense Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
