package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderGender;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderGender extends Processor {
    private static String PROVIDER_GENDER_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_gender where loaderCompositeKey in (";
    private static String PROVIDER_GENDER_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_gender(hcid,company_count,delta_date,gender,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey)VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_GENDER_UPDATE_QUERY = "UPDATE futurerx_provider.provider_gender SET hcid = ?,company_count = ?,delta_date = ?,gender = ?,provider_id = ?,record_type = ?,tier_code = ?,verification_code = ?,verification_date = ?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderGender.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findGenderIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_GENDER_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderGenderList(final List<FuturerxProviderGender> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_GENDER_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderGender pg = records.get(idx);
				int i = 1;
				ps.setString(i++, pg.getHcid());
				ps.setString(i++, pg.getCompanyCount());
				if(null != pg.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getGender());
				ps.setString(i++, pg.getProviderId());
				ps.setString(i++, pg.getRecordType());
				ps.setString(i++, pg.getTierCode());
				ps.setString(i++, pg.getVerificationCode());
				if(null != pg.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pg.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderGenderList(final List<FuturerxProviderGender> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_GENDER_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderGender pg = records.get(idx);
				int i = 1;
				ps.setString(i++, pg.getHcid());
				ps.setString(i++, pg.getCompanyCount());
				if(null != pg.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pg.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getGender());
				ps.setString(i++, pg.getProviderId());
				ps.setString(i++, pg.getRecordType());
				ps.setString(i++, pg.getTierCode());
				ps.setString(i++, pg.getVerificationCode());
				if(null != pg.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pg.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pg.getLoaderCompositeKey());
	            ps.setLong(i++, pg.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderGender> insertRecordsList = new ArrayList<FuturerxProviderGender>();
		List<FuturerxProviderGender> updateRecordsList = new ArrayList<FuturerxProviderGender>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderGender) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderGender) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderGender pa = (FuturerxProviderGender) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if(compositeKeyList.size() > 0) {
			Map<String, Long> genderIdMap = findGenderIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);
		    
	    for (Object paObj: records) {
	    	FuturerxProviderGender pg = (FuturerxProviderGender) paObj;
	    	long id = 0;	
	    	
	    	if (hcidIdMap.containsKey(pg.getHcid())) {
				pg.setHcid(hcidIdMap.get(pg.getHcid()).toString());
				
				if (genderIdMap.containsKey(pg.getLoaderCompositeKey())) {
					id = genderIdMap.get(pg.getLoaderCompositeKey());
				}
				if (id > 0) {
					pg.setId(id);
					updateRecordsList.add(pg);
				} else {
					insertRecordsList.add(pg);
				}
			}
	       }
		}
	    
		try {
	        updateProviderGenderList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderGenderList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider gender Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
