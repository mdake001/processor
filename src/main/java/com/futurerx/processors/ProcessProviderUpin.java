package com.futurerx.processors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.futurerx.FuturerxProcessor;
import com.futurerx.ProcessingCounts;
import com.futurerx.model.dest.FuturerxProviderUPIN;
import com.futurerx.utils.FileLoadDetail;

public class ProcessProviderUpin extends Processor {
    private static String PROVIDER_UPIN_SEARCH_LIST_QUERY = "SELECT loaderCompositeKey, id from futurerx_provider.provider_upin where loaderCompositeKey in (";
    private static String PROVIDER_UPIN_INSERT_QUERY = "INSERT INTO futurerx_provider.provider_upin(hcid,upin,company_count,delta_date,provider_id,record_type,tier_code,verification_code,verification_date,loaderCompositeKey) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static String PROVIDER_UPIN_UPDATE_QUERY = "UPDATE futurerx_provider.provider_upin SET hcid =?,upin =?,company_count =?,delta_date =?,provider_id =?,record_type =?,tier_code =?,verification_code =?,verification_date =?,loaderCompositeKey =? WHERE id = ?";

	private static Logger log = LoggerFactory.getLogger(ProcessProviderUpin.class);
	private static final ApplicationContext aContext=new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final JdbcTemplate jdbcTemplate = (JdbcTemplate) aContext.getBean("jdbcTemplate");

    public static Map<String, Long> findUPINIds(List<String> records) {
    	final Map<String, Long> map = new HashMap<String, Long>();
    	final StringBuilder queryBuilder = new StringBuilder(PROVIDER_UPIN_SEARCH_LIST_QUERY);
    	
	    	for (int i = 0;i< records.size();i++) {
	    		map.put(records.get(i), 0L);    		
	    	}
	
	    	for (int i = 0;i< map.size();i++) {
	    		if (i < map.size()-1) {
	    			queryBuilder.append("?,");
	    		} else {
	    			queryBuilder.append("?)");    			
	    		}
	    	}
    	jdbcTemplate.query(new PreparedStatementCreator() {			
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement ps = con.prepareStatement(queryBuilder.toString());
					int i = 1;
					
					for (String id: map.keySet()) {
						ps.setString(i++, id);
					}
					return ps;
				}
			},
			new RowCallbackHandler() {				
				public void processRow(ResultSet rs) throws SQLException {
					map.put(rs.getString("loaderCompositeKey"), rs.getLong("id"));					
				}
			}  
		);
    	return map;    	
    }
        
    private static void insertProviderUPINList(final List<FuturerxProviderUPIN> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_UPIN_INSERT_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderUPIN pu = records.get(idx);
				int i = 1;
				ps.setString(i++, pu.getHcid());
				ps.setString(i++, pu.getUpin());
				ps.setString(i++, pu.getCompanyCount());
				if(null != pu.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pu.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				
				ps.setString(i++, pu.getProviderId());
				ps.setString(i++, pu.getRecordType());
				ps.setString(i++, pu.getTierCode());
				ps.setString(i++, pu.getVerificationCode());
				if(null != pu.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pu.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pu.getLoaderCompositeKey());
			}
			
			public int getBatchSize() {			
				return records.size();
			}
		});    	
    }

    
    private static void updateProviderUPINList(final List<FuturerxProviderUPIN> records, final Date processingDatetime) throws Exception {
    	jdbcTemplate.batchUpdate(PROVIDER_UPIN_UPDATE_QUERY, new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int idx) throws SQLException {
				FuturerxProviderUPIN pu = records.get(idx);
				int i = 1;
				ps.setString(i++, pu.getHcid());
				ps.setString(i++, pu.getUpin());
				ps.setString(i++, pu.getCompanyCount());
				if(null != pu.getDeltaDate())
					ps.setDate(i++, new java.sql.Date(pu.getDeltaDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				
				ps.setString(i++, pu.getProviderId());
				ps.setString(i++, pu.getRecordType());
				ps.setString(i++, pu.getTierCode());
				ps.setString(i++, pu.getVerificationCode());
				if(null != pu.getVerificationDate())
					ps.setDate(i++, new java.sql.Date(pu.getVerificationDate().getTime()));
				else
					ps.setNull(i++, Types.DATE);
				ps.setString(i++, pu.getLoaderCompositeKey());
	            ps.setLong(i++, pu.getId());  
			}

			public int getBatchSize() {
				return records.size();
			}
		});
    }    

    public ProcessingCounts processList(final List records, Date processingDateTime) throws Exception {
		List<FuturerxProviderUPIN> insertRecordsList = new ArrayList<FuturerxProviderUPIN>();
		List<FuturerxProviderUPIN> updateRecordsList = new ArrayList<FuturerxProviderUPIN>();
		List<String> hcidList = new ArrayList<String>();
		List<String> compositeKeyList = new ArrayList<String>();

		ProcessingCounts c = new ProcessingCounts();
    	String remark = "";
    	Boolean rejectRecord = false;
        List<FileLoadDetail> errorList = new ArrayList<FileLoadDetail>();
		int dataIntakeFileId = ((FuturerxProviderUPIN) records.get(0)).getDataIntakeFileId();
		Long minId = ((FuturerxProviderUPIN) records.get(0)).getRecordNumber().longValue();

		for (Object srcObj: records) {
			FuturerxProviderUPIN pa = (FuturerxProviderUPIN) srcObj;
		    if (!rejectRecord) {
		    	hcidList.add(pa.getHcid());
		    	compositeKeyList.add(pa.getLoaderCompositeKey());
		    }
		    else {
				rejectRecord = false;
		    }
		    
		    if (remark.length() > 0) {
				FileLoadDetail fld = new FileLoadDetail();
				fld.setFileLoadSummaryId(pa.getDataIntakeFileId());
				fld.setRecordNumber(pa.getRecordNumber().longValue());
				fld.setRemark(remark);
				fld.setStatus((fld.getRemark().contains("FAIL")?1:2));
				errorList.add(fld);
				remark = new String();
			}
		}
		if (compositeKeyList.size() > 0) {
			Map<String, Long> upinIdMap = findUPINIds(compositeKeyList);
			Map<String, Long> hcidIdMap = ProcessProviderHCID.findHCIDIds(hcidList);

			for (Object paObj : records) {
				FuturerxProviderUPIN pu = (FuturerxProviderUPIN) paObj;
				long id = 0;

				if (hcidIdMap.containsKey(pu.getHcid())) {
					pu.setHcid(hcidIdMap.get(pu.getHcid()).toString());

					if (upinIdMap.containsKey(pu.getLoaderCompositeKey())) {
						id = upinIdMap.get(pu.getLoaderCompositeKey());
					}
					if (id > 0) {
						pu.setId(id);
						updateRecordsList.add(pu);
					} else {
						insertRecordsList.add(pu);
					}
				}
			}
		}
	    
		try {
	        updateProviderUPINList(updateRecordsList, processingDateTime);
			c.incrementUpdatedRecords(updateRecordsList.size());
	        insertProviderUPINList(insertRecordsList, processingDateTime);
			c.incrementInsertedRecords(insertRecordsList.size());
		} catch (Exception e) {
			log.error("Provider upin Processor Exception: {} ", e.getMessage());
			FileLoadDetail fld = new FileLoadDetail();
			fld.setFileLoadSummaryId(dataIntakeFileId);
			fld.setRecordNumber(minId);
			fld.setRemark("FAIL: Batch error <SIZE:" + records.size() + "> Message: " + e.getMessage());
			fld.setStatus(1);
			errorList.add(fld);
			c.incrementFailed((c.getUpdatedRecords()>0)?insertRecordsList.size()-1:records.size()-1);
		}
		c.incrementFailed(FuturerxProcessor.saveErrors(errorList, jdbcTemplate));
		return c;
    }

}
