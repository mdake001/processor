package com.futurerx;

import java.util.ArrayList;
import java.util.List;

public class FileProcessingErrors {
    private String fileName;
    private List<String> errors;
        
	public FileProcessingErrors(String fileName) {
		super();
		this.fileName = fileName;
		this.errors = new ArrayList<String>();
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}       
	
	public void addError(String error) {
		this.errors.add(error);
	}
}
