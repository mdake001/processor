package com.futurerx;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.futurerx.processors.Processor;

public class ProcessingThread<T1, T2> implements Callable<ProcessingCounts> {
    private List<T1> records;
    private Date processingDateTime;
    private Processor processor;
    
    private static final Logger log = LoggerFactory.getLogger(ProcessingThread.class);
    
    public ProcessingThread(List<T1> records, Date processingDateTime, Class<Processor> processorClass ) {
		super();
		this.records = records;
		this.processingDateTime = processingDateTime;
		try {
			Processor p = processorClass.newInstance();
			this.processor = p;
		} catch (Exception e) {
			log.error("Error creating Processor instance for " + processorClass);
			e.printStackTrace();
		}
	}

    public ProcessingThread(List<T1> records, Date processingDateTime, Processor processor ) {
		super();
		this.records = records;
		this.processingDateTime = processingDateTime;
		this.processor = processor;
	}
    
    
	public ProcessingCounts call() throws Exception {
		ProcessingCounts c = new ProcessingCounts();
		try {
			c.incrementTotal(records.size());
			ProcessingCounts cTemp = processor.processList(records, processingDateTime);
			if (cTemp != null) {
				c.incrementInsertedRecords(cTemp.getInsertedRecords());
				c.incrementUpdatedRecords(cTemp.getUpdatedRecords());
				c.incrementFailed(cTemp.getFailedRecords());
			}
		} catch (Throwable e) {
			c.incrementFailed(records.size());
			c.setException(e.getLocalizedMessage() + System.getProperty("line.separator") + ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
		}
		return c;
	}
}
