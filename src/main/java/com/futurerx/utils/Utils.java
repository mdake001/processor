package com.futurerx.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {
//	public static final String _LIST = "[^\\x20-\\x7e&&[^\u00D1]]";
	public static final String BLACK_LIST = "[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]";
    /*public static void removeDuplicate(List arlList)
    {
	     Set h = new HashSet(arlList);
	     arlList.clear();
	     arlList.addAll(h);
    }*/
    
    public static void setLongParameter(PreparedStatement ps, int index, Long value) throws SQLException {
	    if (value != null) {
	        ps.setLong(index, value);
	    } else {
	    	ps.setNull(index, Types.BIGINT);
	    }
    }

    public static void setDateParameter(PreparedStatement ps, int index, Date value) throws SQLException {
	    if (value != null) {
	        ps.setDate(index, new java.sql.Date(value.getTime()));
	    } else {
	    	ps.setNull(index, Types.DATE);
	    }
    }
	public static String alphanumeralize(String value) {
		if (value != null) {
			return value.replaceAll(BLACK_LIST, "");
		}
		return null;
	}
}
