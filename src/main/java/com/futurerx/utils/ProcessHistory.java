package com.futurerx.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.futurerx.ProcessingCounts;

public class ProcessHistory {
	private static String INSERT_PROCESS_HISTORY_QUERY = "insert into data_loading_audit.process_history (id, process_name, start_time,filename) values (default,?,?,?)";
	private static String UPDATE_PROCESS_HISTORY_QUERY = "update data_loading_audit.process_history set end_time=?,records_processed=?,records_success=?,records_failed=?,exception=? where id=?";

	private long processingHistoryId;
	private JdbcTemplate jdbcTemplate;
	
	public ProcessHistory() {
		super();
	}

	public ProcessHistory(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	public void startProcessing(final String processName, final String filename) {                                                                                              
	    KeyHolder keyHolder = new GeneratedKeyHolder();
	    getJdbcTemplate().update(new PreparedStatementCreator() {
			
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement ps =
	                conn.prepareStatement(INSERT_PROCESS_HISTORY_QUERY, new String[] {"id"});
				ps.setString(1, processName);
				ps.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
				ps.setString(3, filename);
				return ps;
			}
		}
		, keyHolder);
		processingHistoryId = keyHolder.getKey().longValue();
	}

	public void endProcessing(final long totalRecordsCount, final long successCount, final long failedCount, final String exception) {                                                                                              
		String ex = StringUtils.left(exception, 1999); 
		getJdbcTemplate().update(UPDATE_PROCESS_HISTORY_QUERY, new Timestamp(new Date().getTime()), totalRecordsCount, successCount, failedCount, ex, processingHistoryId);
	}	
	
	public void endProcessing(final ProcessingCounts counts) {                                                                                              
		endProcessing(counts.getTotalRecords(), counts.getInsertedRecords(), counts.getFailedRecords(), counts.getException());
    }	

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}                                                                                                                                            

}
