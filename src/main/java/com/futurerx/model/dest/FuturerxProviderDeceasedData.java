package com.futurerx.model.dest;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FuturerxProviderDeceasedData {
	
	private Long id;
	private String hcid;
	private String deceasedIndicator;
	private Date deceasedDate;
	private String providerId;
	private String recordType;
	private Date deltaDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;
}
