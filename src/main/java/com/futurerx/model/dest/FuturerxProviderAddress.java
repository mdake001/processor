package com.futurerx.model.dest;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FuturerxProviderAddress {
    private Long id;
    private String addressId;
    private String hcid;
    private String address1;
    private String address2;
    private String city;
    private String companyCount;
    private String county;
    private Date deltaDate;
    private String dpv;
    private String geoReturn;
    private String latitude;
    private String longitude;
    private String providerId;
    private String addrRank;
    private String recordType;
    private String state;
    private String tierCode;
    private String verificationCode;
    private Date verificationDate;
    private String zip;
    private String zip4;
    private String loaderCompositeKey;
    private Integer dataIntakeFileId;
    private Long recordNumber;
	
}
