package com.futurerx.model.dest;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FuturerxProviderNPI {

	private Long id;
	private String hcid;
	private Date deactivationDate;
	private String companyCount;
	private Date deltaDate;
	private String npi;
	private String npiRank;
	private Date reactivationDate;
	private String providerId;
	private String recordType;
	private String tierCode;
	private String verificationCode;
	private Date verificationDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;
	private String insertUser;
	private Date insertDateTime;
}
