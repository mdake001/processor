package com.futurerx.model.dest;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FuturerxProvider {
	private Long id;
    private String hcid;
    private Long providerId;
    private String companyCount;
    private String recordType;
    private String providerType;
    private Date deltaDate;
    private String loaderCompositeKey;
    private Integer dataIntakeFileId;
    private Long recordNumber;
  
}
