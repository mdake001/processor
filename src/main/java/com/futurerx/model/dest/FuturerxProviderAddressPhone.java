package com.futurerx.model.dest;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FuturerxProviderAddressPhone implements java.io.Serializable {
	private Long id;
	private String addressId;
	private String hcid;
	private String phoneNumber;
	private String phoneType;
	private String companyCount;
	private Date deltaDate;
	private String providerId;
	private String addrPhoneRank;
	private String recordType;
	private String tierCode;
	private String verificationCode;
	private Date verificationDate;
	private Integer dataIntakeFileId;
	private Long recordNumber;
	private String loaderCompositeKey;

}
