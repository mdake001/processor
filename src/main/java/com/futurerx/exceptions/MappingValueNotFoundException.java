package com.futurerx.exceptions;

public class MappingValueNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 309901624985507162L;

	public MappingValueNotFoundException(String exception) {
    	super(exception);    	
    }
}
