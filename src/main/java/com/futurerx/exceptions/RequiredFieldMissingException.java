package com.futurerx.exceptions;

public class RequiredFieldMissingException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2776903045890150737L;
	
	private String fieldName;

	public RequiredFieldMissingException(String fieldName) {
		super();
		this.fieldName = fieldName;
	}

	@Override
	public String getLocalizedMessage() {
		return "Required field " + fieldName + " is missing or empty";
	}

	@Override
	public String getMessage() {		
		return "Required field " + fieldName + " is missing or empty";
	}
    
    
}
