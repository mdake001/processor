package com.futurerx;


public class ProcessingCounts {
	private long totalRecords;
    private long insertedRecords;
    private long updatedRecords;
    private long failedRecords;
    private String exception;
    
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public long getInsertedRecords() {
		return insertedRecords;
	}
	public void setInsertedRecords(long insertedRecords) {
		this.insertedRecords = insertedRecords;
	}
	public long getUpdatedRecords() {
		return updatedRecords;
	}
	public void setUpdatedRecords(long updatedRecords) {
		this.updatedRecords = updatedRecords;
	}
	public long getFailedRecords() {
		return failedRecords;
	}
	public void setFailedRecords(long failedRecords) {
		this.failedRecords = failedRecords;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}

	
    public void incrementInsertedRecords() {
    	insertedRecords++;
    }
    public void incrementInsertedRecords(long incCount) {
    	insertedRecords+=incCount;
    }
    public void incrementUpdatedRecords() {
    	updatedRecords++;
    }
    public void incrementUpdatedRecords(long incCount) {
    	updatedRecords+=incCount;
    }
    public void incrementTotal() {
    	totalRecords++;
    }
    public void incrementTotal(long incCount) {
    	totalRecords+=incCount;
    }
    public void incrementFailed() {
    	failedRecords++;
    }
    public void incrementFailed(long incCount) {
    	failedRecords+=incCount;
    }
	@Override
	public String toString() {
		return "ProcessingCounts [totalRecords=\t" + totalRecords + "\t, insertedRecords=\t" + insertedRecords + "\t, updatedRecords=\t" + updatedRecords + "\t, failedRecords=\t" + failedRecords + "]";
	}
    
}
