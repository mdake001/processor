package com.futurerx;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.futurerx.processors.Processor;

public class AbstractProcessingThread<T1, T2> implements Callable<ProcessingCounts> {
    private List<T1> records;
    private Date processingDateTime;
    private Processor processor;
    
    public AbstractProcessingThread(List<T1> records, Date processingDateTime, Processor processor ) {
		super();
		this.records = records;
		this.processingDateTime = processingDateTime;
		this.processor = processor;
	}
    
	public ProcessingCounts call() throws Exception {
		ProcessingCounts c = new ProcessingCounts();
		try {
			c.incrementTotal(records.size());						
			processor.processList(records, processingDateTime);
			c.incrementInsertedRecords(records.size());
		} catch (Exception e) {
			c.incrementFailed(records.size());
			c.setException(e.getLocalizedMessage() + System.getProperty("line.separator") + ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
		}
		return c;
	}    
}
